function extract_file_name(raw_fn) {
    split(raw_fn,tmp,".")
    n = split(tmp[1],fn,"/")
    return fn[n]
}

function parseAttribute(line, attribute, shift) {
    n = split(line, tmp, attribute"=")
    if (n > 0) {
	split(tmp[2], tmp2, "\"")
	print shift"\""attribute"\": \""tmp2[2]"\","
    }
}

function closeArray(shift) {
    shift = substr(shift, 1, length(shift) -2)
    print shift"],"
    return shift
}

function closeBrackets(shift) {
    shift = substr(shift, 1, length(shift) -2)
    print shift"},"
    return shift
}



BEGIN {
    inBlock = ""
    quantity_of_brackets = 0
    shift = "  "
    file_name = extract_file_name(ARGV[1])
    print shift"{"
    shift = shift"  "
    print shift"\"name\": \""file_name"\","
}

{
    if ($1 == "<procedure") {
	parseAttribute($0, "categories", shift)
	parseAttribute($0, "actions", shift)
	parseAttribute($0, "position", shift)
    }

    if ($1 == "<parameters>") {
	print shift"\"parameters\": {"
	shift = "  "shift
    }

    if ($1 == "</parameters>") {
	if (inBlock) {
	    shift = closeArray(shift)
	}
	    shift = closeBrackets(shift)
	shift = closeBrackets(shift)
    }

    if ($1 == "<group") {
      inHandlerBlock = 0
      print shift"\"group\": {"
      shift = shift"  "
      parseAttribute($0, "name", shift)
    }

    if ($1 == "</group>") {
      shift = substr(shift, 1, length(shift) -2)
      if (inBlock) {
	 shift = closeArray(shift)
      }
      shift = closeBrackets(shift)
      inBlock = ""
    }

    if (($1 == "<target") || ($1 == "<output") || ($1 == "<input") || ($1 == "<doer") || ($1 == "<tool")) {
        block = substr($1, 2, length($1))
	if (!inBlock) {
            print shift"\""block"\": ["
	    inBlock = block
            shift = "  "shift
	 } else {
	     if (inBlock != block) {
                 shift = closeArray(shift)
		 print shift"\""block"\": ["
                 shift = "  "shift
	         inBlock = block
	      }
	  }
     
	print shift"{"
        shift = "  "shift
	parseAttribute($0, "name", shift)
	parseAttribute($0, "filter", shift)
	parseAttribute($0, "cardinality", shift)
        
	if (substr($NF, length($NF) -1, length($NF)) == "/>") {
	    shift = closeBrackets(shift)
	}
    }

    if (($1 == "</target>") || ($1 == "</output>") || ($1 == "</input>") || ($1 == "</doer") || ($1 == "</tool")) {
	if (inHandlerBlock) {
	    inHandlerBlock = !inHandlerBlock
	    shift = closeBrackets(shift)
	    shift = closeArray(shift)
	}
	shift = closeBrackets(shift)
    }

    if ($1 == "<handler") {
	if (!inHandlerBlock) {
	    print shift"\"handler\": ["
	    shift = "  "shift
	    inHandlerBlock = 1
	} else {
	    shift = closeBrackets(shift)
	}
        print shift"{"
	shift = "  "shift
	
	parseAttribute($0, "name", shift)
	parseAttribute($0, "indicator", shift)
	parseAttribute($0, "unit", shift)
    }
}
