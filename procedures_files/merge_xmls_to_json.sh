#!/bin/bash
rm procedures.json
echo "Merging procedures files... "
echo "[" >> procedures.json
#awk -f xml_to_json.awk procedures/all_in_one_sowing.xml >> procedures.json
find procedures -name "*.xml" -exec awk -f xml_to_json.awk {} \; >> procedures.json
echo "]" >> procedures.json
#cat procedures.json
sed -i.bak ':begin;$!N;s/,\n}/\n}/g;tbegin;P;D' procedures.json
cp procedures.json ../app/src/main/assets/procedures/json/
echo "Output file generated : procedures.json copied into app/src/main/assets"
