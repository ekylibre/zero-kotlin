package com.ekylibre.android.zero.ui.fragments.accountList

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.ekyapicom.sdk.database.models.AccountDB
import com.ekylibre.android.zero.R
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_account.view.*
import javax.inject.Inject

class AccountsAdapter @Inject constructor(@ActivityContext private val context: Context, private val ekyAPIComSDK: EkyAPIComSDK) : RecyclerView.Adapter<AccountsAdapter.AccountsListViewHolder>() {

    class AccountsListViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView) {}

    private var accountDataset: ArrayList<AccountDB> = ArrayList()

    fun setData(newData: List<AccountDB>) {
        this.accountDataset.clear()
        this.accountDataset.addAll(newData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountsListViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context).inflate(R.layout.item_account, parent, false) as CardView
        return AccountsListViewHolder(constraintLayout)
    }

    override fun onBindViewHolder(holder: AccountsListViewHolder, position: Int) {
        holder.cardView.visibility = View.VISIBLE
        if (accountDataset[position].a_id == ekyAPIComSDK.getCurrentAccount()?.a_id) {
            holder.cardView.visibility = View.GONE
        } else {
            holder.cardView.account_name_tv.text = accountDataset[position].username
            holder.cardView.current_account_api_url_tv.text = accountDataset[position].api_url
            holder.cardView.switch_current_account_button.setOnClickListener {  ekyAPIComSDK.switchCurrentAccount(accountDataset[position].a_id) }
            holder.cardView.delete_account_button.setOnClickListener {
                ekyAPIComSDK.deleteAccount(accountDataset[position])
                accountDataset.removeAt(position)
                holder.cardView.visibility = View.GONE
                notifyItemRemoved(position)
                notifyItemRangeChanged(position, accountDataset.size)
                notifyDataSetChanged()
            }
            holder.cardView.current_account_id.text = "# " + accountDataset[position].a_id
        }
    }

    override fun getItemCount(): Int {
        return accountDataset.size
    }
}