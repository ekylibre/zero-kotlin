package com.ekylibre.android.zero.ui.activities.detailedintervention

import android.content.Context
import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.ekylibre.android.ekyapicom.sdk.Constants
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterWithProductDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionWithParameterAndProductDB
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.LandParcel
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.NetSurfaceArea
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.Plant
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionResponse
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.Parameter
import com.ekylibre.android.zero.constants.ProcedureCategories
import com.ekylibre.android.zero.constants.ProcedureFamilies

class DetailedInterventionViewModel@ViewModelInject constructor(val ekyAPIComSDK: EkyAPIComSDK): ViewModel() {

    var interId: Long = 0
    var inter: InterventionWithParameterAndProductDB? = null
    var totalArea: NetSurfaceArea? = null

    fun loadInterventionDetails(id: Long) {
        this.interId = id
        this.inter = ekyAPIComSDK.getRemoteInterventionFromDB(interId)
    }

    fun getLocalInterventionError(id: Long): String {
        val localInters = ekyAPIComSDK.getLocalInterventionsFromDB()
        return localInters.toList().find { it.id == id }?.error ?: ""
    }

    fun delete() {
        ekyAPIComSDK.deleteIntervention(interId)
    }

    fun getProcedureFamily(procedure: String): Int? {
        val catstring = ekyAPIComSDK.getAllSimplifiedProcedures().associateBy(ProcedureDB::name).get(procedure)?.categories ?: ""
        val catobj = ProcedureCategories.getByCategoryName(catstring.split(",")[0])
        val family = ProcedureFamilies.procedureFamilies.entries.find { it.value.contains(catobj) }?.key
        return family
    }

    fun getTargetsDetails(c: Context): Map<Long, String> {
        val targets = (this.inter?.interventionParameters?.groupBy { it.parameter.role })?.get("target")
        val res = HashMap<Long, String>()

        targets?.map { p ->
            val productId = p.interventionProduct?.getProductId()
            val n = productId?.let { getSurfaceArea(p.interventionProduct) }
            n?.let {
                if (this.totalArea == null) this.totalArea = NetSurfaceArea(n.value, n.unit)
                else this.totalArea?.value = (this.totalArea?.value!!.toDouble() + n.value.toDouble()).toString()
                res.put(productId, n.value + " " + Constants.getUnitString(n.unit, c))
            }
        }
        return res
    }

    fun getGroupedParams(): Map<String, List<InterventionParameterWithProductDB>>? {
        return this.inter?.interventionParameters?.groupBy { it.parameter.role }
    }

    private fun getSurfaceArea(product: InterItem?): NetSurfaceArea? {
        return when (product) {
            is LandParcel -> product.net_surface_area
            is Plant -> product.net_surface_area
            else -> null
        }
    }
}