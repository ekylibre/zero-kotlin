package com.ekylibre.android.zero.ui.fragments.procedureform.datepicker

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.format.DateFormat
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import java.util.*

class TimePickerFragment(var listener: OnTimeAdjustedListener, val c: Calendar) : DialogFragment(), TimePickerDialog.OnTimeSetListener {

    interface OnTimeAdjustedListener {
        fun onTimeAdjusted(tag: String?, hour: Int, minute: Int)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        return TimePickerDialog(activity, this, hour, minute, DateFormat.is24HourFormat(activity))
    }

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        this.listener.onTimeAdjusted(tag, hourOfDay, minute)
    }
}