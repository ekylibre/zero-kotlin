package com.ekylibre.android.zero.constants

import com.ekylibre.android.zero.R

enum class ProcedureCategories(val id: Int, val categoryName: String, val colorId: Int) {
    ADMINISTRATIVE_TASK(R.string.administrative_tasks, "administrative_tasks", R.color.administrative_task),
    ANIMAL_FEEDING(R.string.animal_feeding, "animal_feeding", R.color.animal_feeding),
    ANIMAL_TREATING(R.string.animal_treating, "animal_treating", R.color.animal_treating),
    ANIMAL_PRODUCTION(R.string.animal_production, "animal_production", R.color.animal_production),
    ANIMAL_BREEDING(R.string.animal_breeding, "animal_breeding", R.color.animal_breeding),
    ANIMAL_IDENTIFICATION(R.string.animal_identification, "animal_identification", R.color.animal_identification),
    SOIL_WORKING(R.string.soil_working, "soil_working", R.color.soil_working),
    HARVESTING(R.string.harvesting, "harvesting", R.color.harvesting),
    CROP_MAINTENANCE(R.string.crop_maintenance, "crop_maintenance", R.color.crop_maintenance),
    CROP_PROTECTION(R.string.crop_protection, "crop_protection", R.color.crop_protection),
    PLANTING(R.string.planting, "planting", R.color.planting),
    IRRIGATING(R.string.irrigating, "irrigating", R.color.irrigating),
    FERTILIZING(R.string.fertilizing, "fertilizing", R.color.fertilizing),
    HANDLING(R.string.handling, "handling", R.color.handling),
    EQUIPMENT_MAINTENANCE(R.string.equipment_maintenance, "equipment_maintenance", R.color.equipment_maintenance),
    HABITAT_MAINTENANCE(R.string.habitat_maintenance, "habitat_maintenance", R.color.habitat_maintenance),
    INSTALLATIONS_MODIFICATION(R.string.installations_modification, "installations_modification", R.color.installations_modification),
    ENERGY_SUPPLYING(R.string.energy_supplying, "energy_supplying", R.color.energy_supplying),
    PLANTS_TRANSFORMATION(R.string.plants_transformation, "plants_transformation", R.color.plants_transformation),
    VITICULTURE_TRANSFORMATION(R.string.viticulture_transformation, "viticulture_transformation", R.color.viticulture_transformation),
    PACKAGING(R.string.packaging, "packaging", R.color.packaging),
    VINEYARD_MAINTENANCE(R.string.vineyard_maintenance, "vineyard_maintenance", R.color.vineyard_maintenance),
    VINE_SOIL_WORKING_AND_MAINTENANCE(R.string.soil_working_and_maintenance, "vine_soil_working_and_maintenance", R.color.vine_soil_working_and_maintenance),
    VINE_PROTECTION(R.string.vine_protection, "vine_protection", R.color.vine_protection),
    VINE_PLANTING(R.string.vine_planting, "vine_planting", R.color.vine_planting),
    VINE_IRRIGATING(R.string.irrigating, "vine_irrigating", R.color.vine_irrigating),
    VINE_FERTILIZING(R.string.fertilizing, "vine_fertilizing", R.color.vine_fertilizing),
    TRELLISING(R.string.trellising, "trellising", R.color.trellising);

    companion object {
        private val map = ProcedureCategories.values().associateBy(ProcedureCategories::categoryName)
        fun getByCategoryName(name: String) = map[name]
    }
}