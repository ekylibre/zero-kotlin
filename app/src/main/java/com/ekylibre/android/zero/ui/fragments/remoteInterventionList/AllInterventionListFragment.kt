package com.ekylibre.android.zero.ui.fragments.remoteInterventionList

import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class AllInterventionListFragment: InterventionListFragment() {
    override var isAll = true
}