package com.ekylibre.android.zero.ui.views

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.ekylibre.android.zero.R
import kotlinx.android.synthetic.main.item_option_sync.view.*

class SyncOptionItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
): ConstraintLayout(context, attrs, defStyle, defStyleRes) {

    init {
        LayoutInflater.from(context).inflate(R.layout.item_option_sync, this, true)

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.SyncOptionItemView,
                0,
                0
            )

            setTitle(typedArray)
            setId(typedArray)

            typedArray.recycle()
        }
    }

    private fun setTitle(typedArray: TypedArray) {
        val title = resources.getText(
            typedArray.getResourceId(
                R.styleable.SyncOptionItemView_item_option_sync_title,
                R.string.no_title
            )
        )

        item_sync_option_title.text = title
    }

    private fun setId(typedArray: TypedArray) {
        val id = typedArray.getResourceId(
            R.styleable.SyncOptionItemView_item_option_sync_button_id,
            R.string.no_title
        )

        item_sync_option_button.id = id
    }
}