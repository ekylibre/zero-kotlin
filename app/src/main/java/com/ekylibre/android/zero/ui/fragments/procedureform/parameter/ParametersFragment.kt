package com.ekylibre.android.zero.ui.fragments.procedureform.parameter

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionViewModel

abstract class ParametersFragment: Fragment() {

    internal val model: NewInterventionViewModel by activityViewModels()

    abstract fun initAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    internal fun initRecyclerView(root: View, viewAdapter: ParameterCategoryAdapter) {
        val viewManager = LinearLayoutManager(context)
        root.findViewById<RecyclerView>(R.id.detailed_inter_param_adapter).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }
}