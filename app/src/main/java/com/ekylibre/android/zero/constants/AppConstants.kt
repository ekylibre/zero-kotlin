package com.ekylibre.android.zero.constants

class AppConstants {

    companion object {
        val WORKER = "worker"
        val CHILD = "child"
        val LAND_PARCEL= "land_parcel"
        val EQUIPMENT = "equipment"
        val ANIMAL = "animal"
        val MATTER = "matter"
        val DOER = "doer"
        val PLANT = "plant"
        val OUTPUT = "output"
        val PHYTO = "phyto"
        val FERTILIZER = "fertilizer"
        val INPUT = "input"
        val TARGET = "target"
        val TOOL = "tool"
        val REQUEST = "request"
        val RECORD = "record"
        val IN_PROGRESS = "in_progress"
        val VALIDATED = "validated"
    }
}