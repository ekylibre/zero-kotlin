package com.ekylibre.android.zero

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.edit
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.zero.ui.fragments.AddAccountDialogFragment
import com.ekylibre.android.zero.ui.fragments.BottomSheetModalFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*


@AndroidEntryPoint
class MainActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        val username = com.ekylibre.android.ekyapicom.sdk.Utils.readStringInSharedPreferences("username", this)
        val theme = com.ekylibre.android.ekyapicom.sdk.Utils.readBooleanInSharedPreferences(username + "_theme", this)

        if ((theme && AppCompatDelegate.getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_YES) ||
            (!theme && AppCompatDelegate.getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_NO)) {
            AppCompatDelegate.setDefaultNightMode(if (theme) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
            delegate.applyDayNight()
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getSharedPreferences(packageName, 0)
            ?.registerOnSharedPreferenceChangeListener(this)

        val navController = findNavController(R.id.nav_host_fragment)

        navigation_view.setupWithNavController(navController)
        setupFab()

        updateProceduresIfNeeded()
    }

    private fun updateProceduresIfNeeded() {
        val currentlyStoredVersionCode = com.ekylibre.android.ekyapicom.sdk.Utils.readIntInSharedPreferences("app_version_code", applicationContext)
        if (currentlyStoredVersionCode != BuildConfig.VERSION_CODE) {
            EkyAPIComSDK(applicationContext).refreshProceduresInDB()
            com.ekylibre.android.ekyapicom.sdk.Utils.saveInSharedPreferences("app_version_code", BuildConfig.VERSION_CODE, applicationContext)
        }
    }

    private fun setupFab() {
        fab.setOnClickListener {
            BottomSheetModalFragment().apply {
                show(supportFragmentManager, tag)
            }
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == "authentication_error") {
            val errorMessage = sharedPreferences?.getString(key, null)
            errorMessage?.let {
                Utils.displaySnackBar(it, navigation_view, getString(R.string.retry)) { showAddAccountDialog() }
                sharedPreferences.edit { remove(key) }
            }
        } else if (key == "sync_error") {
            val errorMessage = sharedPreferences?.getString(key, null)
            Log.d(key, errorMessage ?: "")
            errorMessage?.let {
                Utils.displaySnackBar(it, navigation_view, getString(R.string.retry)) {
                    //TODO the value doesn't change value and doesn't trigger the expected fun
                    val key = sharedPreferences?.getString("username", null)+"_inter_to_sync"
                    com.ekylibre.android.ekyapicom.sdk.Utils.saveInSharedPreferences(key, com.ekylibre.android.ekyapicom.sdk.Utils.readStringInSharedPreferences(key, this), this)
                }
                sharedPreferences.edit { remove(key) }
            }
        }
    }

    private fun showAddAccountDialog() {
        val dialog = AddAccountDialogFragment()
        dialog.show(supportFragmentManager, "LoginFragment")
    }
}