package com.ekylibre.android.zero.ui.fragments.procedureform.parameter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.GenericProductParameter
import com.ekylibre.android.zero.R
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_parameter_category.view.*
import javax.inject.Inject

open class ParameterCategoryAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<ParameterCategoryAdapter.ParameterCategoriesListViewHolder>() {

    class ParameterCategoriesListViewHolder(val view: ConstraintLayout): RecyclerView.ViewHolder(view)

    open var isGroup: Boolean = false
    open var groupId: Int = 0
    protected var dataset: ArrayList<String> = arrayListOf("target", "input", "output", "tool", "doer")
    private lateinit var paramsMap: Map<String, List<GenericProductParameter>>

    open fun setData(data: Map<String, List<GenericProductParameter>>, isGroup: Boolean, groupId: Int) {
        val tmp = ArrayList(dataset)
        tmp.map { if (!data.keys.contains(it)) dataset.remove(it) }
        this.paramsMap = data
        notifyDataSetChanged()

       this.isGroup = isGroup
       this.groupId = groupId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParameterCategoriesListViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_parameter_category, parent, false) as ConstraintLayout
        return ParameterCategoriesListViewHolder(constraintLayout)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(holder: ParameterCategoriesListViewHolder, position: Int) {
        this.paramsMap[dataset[position]]?.let { initRecyclerView(holder.view.dialog_products_recycler_view, it, dataset[position]) }
    }

    private fun initRecyclerView(procedureListRecycler: RecyclerView, data: List<GenericProductParameter>, key: String) {
        val viewManager = LinearLayoutManager(context)

        val paramListAdapter = ParameterAdapter(context)
        paramListAdapter.setData(data, key, isGroup, groupId)
        procedureListRecycler.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = paramListAdapter
        }
    }
}