package com.ekylibre.android.zero.ui.fragments.procedureform.parameter.group

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.GenericProductParameter
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.ui.fragments.procedureform.parameter.ParameterAdapter
import com.ekylibre.android.zero.ui.fragments.procedureform.parameter.ParameterCategoryAdapter
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_parameter_category.view.*
import kotlinx.android.synthetic.main.item_parameter_category.view.dialog_products_recycler_view
import kotlinx.android.synthetic.main.item_parameter_category_group.view.*
import javax.inject.Inject

class GroupParameterCategoryAdapter @Inject constructor(@ActivityContext private val context: Context) : ParameterCategoryAdapter(context) {

    override var isGroup = true
    private lateinit var groupParamsMap: List<Map<String, List<GenericProductParameter>>>
    private lateinit var rlistener: (position: Int) -> Unit

    override fun setData(data: Map<String, List<GenericProductParameter>>, isGroup: Boolean, groupId: Int) {
        super.setData(data, isGroup, groupId)
        this.groupParamsMap = listOf(data)
    }

    fun setGroupData(data: List<Map<String, List<GenericProductParameter>>>) {
        this.groupParamsMap = data
        notifyDataSetChanged()
    }

    fun setRemoveListener(listener: (position: Int) -> Unit) {
        rlistener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParameterCategoriesListViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_parameter_category_group, parent, false) as ConstraintLayout
        return ParameterCategoriesListViewHolder(constraintLayout)
    }

    override fun getItemCount(): Int {
        return groupParamsMap.size
    }

    override fun onBindViewHolder(holder: ParameterCategoriesListViewHolder, position: Int) {
        val viewManager = LinearLayoutManager(context)

        val paramListAdapter = ParameterCategoryAdapter(context)
        paramListAdapter.setData(groupParamsMap[position], true, position)
        holder.view.dialog_products_recycler_view.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = paramListAdapter
        }

        holder.view.group_remove.apply {
            setOnClickListener {
                rlistener(position)
            }
        }
    }
}