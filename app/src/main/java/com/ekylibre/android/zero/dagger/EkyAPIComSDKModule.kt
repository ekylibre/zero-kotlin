package com.ekylibre.android.zero.dagger

import android.app.Application
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.zero.ZeroApplication
import dagger.Module
import dagger.Provides
import dagger.hilt.DefineComponent
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object EkyAPIComSDKModule {

    @Singleton
    @Provides
    fun provideEkyAPIComSDK(app: Application): EkyAPIComSDK {
        return EkyAPIComSDK(app.applicationContext)
    }
}