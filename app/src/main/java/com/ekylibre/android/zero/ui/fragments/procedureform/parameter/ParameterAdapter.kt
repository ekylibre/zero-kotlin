package com.ekylibre.android.zero.ui.fragments.procedureform.parameter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.filter.Onthology
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.GenericProductParameter
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.HandlerParameter
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.constants.AppConstants
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import com.ekylibre.android.zero.ui.fragments.procedureform.SelectedNewBornAdapter
import com.ekylibre.android.zero.ui.fragments.procedureform.SelectedPhytoProductsAdapter
import com.ekylibre.android.zero.ui.fragments.procedureform.SelectedProductsWithQuantityAdapter
import com.ekylibre.android.zero.ui.fragments.procedureform.SimpleSelectedProductsAdapter
import com.ekylibre.android.zero.ui.fragments.procedureform.productlist.ProductListDialogFragment
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_parameter.view.*
import javax.inject.Inject

class ParameterAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<ParameterAdapter.ParameterListViewHolder>() {

    class ParameterListViewHolder(val view: ConstraintLayout): RecyclerView.ViewHolder(view)

    private var isGroup: Boolean = false
    private var groupId: Int = 0
    private var dataset: ArrayList<GenericProductParameter> = ArrayList()
    private var key: String = ""

    fun setData(data: List<GenericProductParameter>, key: String, isGroup: Boolean, groupId: Int) {
        dataset.clear()
        dataset.addAll(data)
        this.key = key
        this.isGroup = isGroup
        this.groupId = groupId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParameterListViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_parameter, parent, false) as ConstraintLayout
        return ParameterListViewHolder(constraintLayout)
    }

    override fun onBindViewHolder(holder: ParameterListViewHolder, position: Int) {
        holder.view.add_button.text = "${context.getString(R.string.add_one)} ${Utils.getDisplayString(dataset[position].name ?: "", context)}"
        holder.view.add_button.icon = Utils.getDisplayIcon(dataset[position].name ?: "", context)
        holder.view.add_button.setOnClickListener { showProductListDialog(dataset[position]) }
        dataset[position].handler?.map { if (it.indicator == "shape") dataset[position].handler!!.remove(it) }
        dataset[position].name?.let { initRecyclerView(holder.view.selected_products_recycler, it, dataset[position].handler?.toList() ?: listOf()) }
        if (isGroup)
            holder.view.divider3.visibility = View.GONE
    }

    private fun showProductListDialog(genericProductParameter: GenericProductParameter) {
        val tmp = if (isGroup)
            "$key@group"
        else
            key

        var filter = genericProductParameter.filter?.split("and has indicator shape")?.joinToString("")?.replace("\\s+".toRegex(), " ")
        if (filter == " ") filter = ""

        if (genericProductParameter.name == "cultivation") {
            AlertDialog.Builder(context)
                .setTitle(R.string.select_cultivation)
                .setPositiveButton(
                    R.string.plant
                ) { _, _ ->
                    val dialog = ProductListDialogFragment.newInstance(tmp, "is plant ", "cultivation", groupId)
                    dialog.show((context as AppCompatActivity).supportFragmentManager, "InterventionFormFragment")
                }
                .setNegativeButton(
                    R.string.land_parcel
                ) { _, _ ->
                    val dialog = ProductListDialogFragment.newInstance(tmp, "is land_parcel ", "cultivation", groupId)
                    dialog.show((context as AppCompatActivity).supportFragmentManager, "InterventionFormFragment")
                }
                .show()
        } else {

            val dialog = ProductListDialogFragment.newInstance(tmp, filter, genericProductParameter.name, groupId)
            dialog.show((context as AppCompatActivity).supportFragmentManager, "InterventionFormFragment")
        }
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    private fun initRecyclerView(selectedProductsRecycler: RecyclerView, name: String, handlers: List<HandlerParameter>) {
        lateinit var viewManager: LinearLayoutManager
        val selectedProductsLiveData = (context as NewInterventionActivity).getSelectedProductsLiveData(this.key, name, isGroup, groupId)

        lateinit var selectedProductsAdapter: RecyclerView.Adapter<*>
        when {
            name == AppConstants.CHILD -> {
                selectedProductsAdapter = SelectedNewBornAdapter(context).apply { setData(selectedProductsLiveData.value!!, key, name, isGroup, groupId) }
                selectedProductsLiveData.observe(context, { (selectedProductsAdapter as SelectedNewBornAdapter).setData(selectedProductsLiveData.value!!, key, name, isGroup, groupId) })
                viewManager = LinearLayoutManager(context)
            }
            handlers.isEmpty() -> {
                selectedProductsAdapter = SimpleSelectedProductsAdapter(context).apply { setData(selectedProductsLiveData.value!!, key, name, isGroup, groupId) }
                selectedProductsLiveData.observe(context, { (selectedProductsAdapter as SimpleSelectedProductsAdapter).setData(selectedProductsLiveData.value!!, key, name, isGroup, groupId) })
                viewManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
            key == AppConstants.INPUT && name == "plant_medicine" -> {
                selectedProductsAdapter = SelectedPhytoProductsAdapter(context).apply { setData(selectedProductsLiveData.value!!, handlers, context.getLoadedUsages(), key, name, isGroup, groupId) }
                selectedProductsLiveData.observe(context, { (selectedProductsAdapter as SelectedPhytoProductsAdapter).setData(selectedProductsLiveData.value!!, handlers, context.getLoadedUsages(), key, name, isGroup, groupId) })
                viewManager = LinearLayoutManager(context)
            }
            else -> {
                selectedProductsAdapter = SelectedProductsWithQuantityAdapter(context).apply { setData(selectedProductsLiveData.value!!, handlers, key, name, isGroup, groupId) }
                selectedProductsLiveData.observe(context, { selectedProductsAdapter.setData(selectedProductsLiveData.value!!, handlers, key, name, isGroup, groupId) })
                viewManager = LinearLayoutManager(context)
            }
        }

        selectedProductsRecycler.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = selectedProductsAdapter
        }
    }
}