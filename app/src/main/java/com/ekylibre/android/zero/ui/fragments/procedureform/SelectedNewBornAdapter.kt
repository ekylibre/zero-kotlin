package com.ekylibre.android.zero.ui.fragments.procedureform

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.Constants
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.HandlerParameter
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.item_selected_new_born.view.*
import javax.inject.Inject


class SelectedNewBornAdapter @Inject constructor(private val context: Context) : RecyclerView.Adapter<SelectedNewBornAdapter.SelectedNewBornViewHolder>(), AdapterView.OnItemSelectedListener {

    class SelectedNewBornViewHolder(val view: ConstraintLayout): RecyclerView.ViewHolder(view)

    private lateinit var name: String
    private lateinit var key: String
    private lateinit var dataset: ArrayList<InterItem>
    private var isGroup: Boolean = false
    private var groupId: Int = 0

    fun setData(data: List<InterItem>, key: String, name: String, isGroup: Boolean, groupId: Int) {
        this.dataset = data as ArrayList
        this.isGroup = isGroup
        this.groupId = groupId

        this.key = key
        this.name = name

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectedNewBornViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_selected_new_born, parent, false) as ConstraintLayout
        return SelectedNewBornViewHolder(view)
    }

    override fun onBindViewHolder(holder: SelectedNewBornViewHolder, position: Int) {
        holder.view.selected_new_born_type.text = dataset[position].getProductName()
        holder.view.selected_new_born_delete.setOnClickListener { (context as NewInterventionActivity).deleteSelectedProduct(key, name, dataset[position].getProductId(), this.isGroup, this.groupId) }

        initSpinners(holder.view, position)
        initTextFields(holder.view, position)

        holder.view.selected_new_born_healthy.setOnCheckedChangeListener { buttonView, isChecked ->
            (context as NewInterventionActivity).setNewBornDetails(dataset[position].getProductId(), null, null, null, null, isChecked, key, name, isGroup, groupId)
        }
    }

    private fun initTextFields(view: ConstraintLayout, position: Int) {
        view.selected_new_born_id_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as NewInterventionActivity).setNewBornDetails(dataset[position].getProductId(), null, s.toString().toLong(), null, null, null, key, name, isGroup, groupId)
            }
        })

        view.selected_new_born_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as NewInterventionActivity).setNewBornDetails(dataset[position].getProductId(), s.toString(), null, null, null, null, key, name, isGroup, groupId)
            }
        })

        view.selected_new_born_mass.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as NewInterventionActivity).setQuantity(dataset[position].getProductId(), s.toString()/*, null*/, null, key, name, isGroup, groupId)
            }
        })

        view.selected_new_born_mass.setText("0.0")
    }

    private fun initSpinners(view: ConstraintLayout, position: Int) {
        initMassUnitSpinnerListener(view.selected_new_born_mass_unit_spinner, dataset[position].getProductId())
        initSpinnerData(view.selected_new_born_mass_unit_spinner, dataset[position].getProductId(), Constants.netMassUnits.values.toMutableList().map{ context.getString(it) })

        initSexSpinnerListener(view.selected_new_born_sex_spinner, dataset[position].getProductId())
        initSpinnerData(view.selected_new_born_sex_spinner, dataset[position].getProductId(), Constants.genderMap.values.toMutableList().map{ context.getString(it) })

        initBirthCdtSpinnerListener(view.selected_new_born_parturition_condition_spinner, dataset[position].getProductId())
        initSpinnerData(view.selected_new_born_parturition_condition_spinner, dataset[position].getProductId(), Constants.birthConditionMap.values.toMutableList().map{ context.getString(it) })
    }

    private fun initBirthCdtSpinnerListener(s: AutoCompleteTextView, productId: Long) {
        s.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                (context as NewInterventionActivity).setNewBornDetails(productId, null, null, null, s.toString(), null, key, name, isGroup, groupId)
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    private fun initSexSpinnerListener(s: AutoCompleteTextView, productId: Long) {
        s.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                (context as NewInterventionActivity).setNewBornDetails(productId, null, null, s.toString(), null, null, key, name, isGroup, groupId)
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    private fun initMassUnitSpinnerListener(s: AutoCompleteTextView, productId: Long) {
       s.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                (context as NewInterventionActivity).setQuantity(productId, null, HandlerParameter(s.toString(), "", ""), key, name, isGroup, groupId)
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    private fun initSpinnerData(s: AutoCompleteTextView, productId: Long, dataset: List<String>) {
        s.tag = productId
        s.threshold = 100
        val arrayAdapter = ArrayAdapter(context, R.layout.item_spinner, dataset).apply { setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) }
        s.setAdapter(arrayAdapter)
        s.setText(dataset[0])
        (context as NewInterventionActivity).setQuantity(productId, null, HandlerParameter(s.text.toString(), "", ""), key, name, isGroup, groupId)
    }

    private fun initQuantityEdit(et: EditText, productId: Long) {
        et.tag = productId
        et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val q = s.toString()
                if (q != "")
                    if (q.toDouble() >= 0) {
                        (context as NewInterventionActivity).setQuantity(productId, q, null, key, name, isGroup, groupId)
                        (et.parent.parent as TextInputLayout).error = null
                    } else (et.parent.parent as TextInputLayout).error = Utils.getDisplayString("can_not_be_negative", context)
            }
        })
    }



    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        arg0.setSelection(position)
        //(context as NewInterventionActivity).setQuantity(arg0.tag as Long, null, arg0.getItemAtPosition(position) as String, null, key, name, isGroup, groupId)
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {}

    override fun getItemCount(): Int {
        return this.dataset.size
    }
}