package com.ekylibre.android.zero.ui.fragments.procedureform

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.Constants
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.Matter
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PhytoUsage
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.HandlerParameter
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.item_selected_phyto.view.*
import kotlinx.android.synthetic.main.item_selected_product_with_quantity.view.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class SelectedPhytoProductsAdapter @Inject constructor(private val context: Context) : RecyclerView.Adapter<SelectedPhytoProductsAdapter.SelectedPhytoViewHolder>(), AdapterView.OnItemSelectedListener {

    class SelectedPhytoViewHolder(val view: ConstraintLayout): RecyclerView.ViewHolder(view)

    private lateinit var name: String
    private lateinit var key: String
    private var selectedUsage: PhytoUsage? = null
    private lateinit var dataset: ArrayList<InterItem>
    private var handlers: List<HandlerParameter> = listOf()
    private var unitsDataset: ArrayList<StringMap<String, Any>> = arrayListOf()
    private lateinit var usages: HashMap<Long, List<PhytoUsage>>
    private var isGroup: Boolean = false
    private var groupId: Int = 0

    fun setData(data: List<InterItem>, handlers: List<HandlerParameter>, usages: Map<Long, List<PhytoUsage>>, key: String, name: String, isGroup: Boolean, groupId: Int, update: Boolean = false) {
        this.dataset = data as ArrayList
        this.handlers = handlers
        this.isGroup = isGroup
        this.groupId = groupId
        this.usages = HashMap(usages)

        this.key = key
        this.name = name
        notifyDataSetChanged()

        this.unitsDataset.clear()
        this.handlers.forEach {
            this.unitsDataset.add(StringMap.create(mapOf("name" to Constants.getHandlerString(it, context), "value" to it)))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectedPhytoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_selected_phyto, parent, false) as ConstraintLayout
        return SelectedPhytoViewHolder(view)
    }

    override fun onBindViewHolder(holder: SelectedPhytoViewHolder, position: Int) {
        holder.view.selected_phyto_name.text = dataset[position].getProductName()
        holder.view.selected_phyto_with_quantity_delete.setOnClickListener { (context as NewInterventionActivity).deleteSelectedProduct(key, name, dataset[position].getProductId(), this.isGroup, this.groupId) }

        initUsagesSpinner(holder.view.selected_usage_spinner, holder.view.selected_phyto_unit_spinner, dataset[position])
        initQuantityEdit(holder.view.selected_phyto_quantity_edit, dataset[position])
    }

    private fun initQuantityEdit(et: EditText, product: InterItem) {
        et.setText(product.getQuantity().toString() ?: "0.0")
        et.tag = product.getProductId()
        et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val q = s.toString()
                if (q != "")
                    selectedUsage?.dose_quantity?.let {
                        if (q.toDouble() <= it.toDouble()) {
                            (context as NewInterventionActivity).setQuantity(product.getProductId(), q, null, key, name, isGroup, groupId)
                            (et.parent.parent as TextInputLayout).error = null
                        }
                        else (et.parent.parent as TextInputLayout).error = Utils.getDisplayString("forbidden_dose", context)
                    }
            }
        })
        et.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                if (et.text.toString() == "0.0")
                    et.setText("")
            }
        }
    }

    private fun initUsagesSpinner(usagesSpinner: AutoCompleteTextView, unitsSpinner: AutoCompleteTextView, product: InterItem) {
        val productId = product.getProductId()
        initUnitSpinner(unitsSpinner, productId)

        usagesSpinner.tag = productId
        usagesSpinner.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {
                selectedUsage = usages[usagesSpinner.tag]?.filter { it.ephy_usage_phrase == s.toString() }?.get(0)
                (product as Matter).setUsageId(selectedUsage?.id)
                setUnitSpinnerDataset(unitsSpinner, product)
            }
        })

        val usages = this.usages[productId]?.map { it.ephy_usage_phrase } ?: ArrayList()
        Collections.sort(usages)
        val arrayAdapter = ArrayAdapter(context, R.layout.item_spinner, usages)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        usagesSpinner.threshold = 100
        usagesSpinner.setAdapter(arrayAdapter)
        if (usages.isNotEmpty()) {
            val items = if ((product as Matter).getUsageId() != null) this.usages[productId]?.filter { it.id == (product as Matter).getUsageId() } else listOf()
            usagesSpinner.setText(if(items?.size == 1) items[0].ephy_usage_phrase else usages[0])
        }
        setUnitSpinnerDataset(unitsSpinner, product)
    }

    private fun initUnitSpinner(spinner: AutoCompleteTextView, productId: Long) {
        spinner.tag = productId
        spinner.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {
                var v = unitsDataset[0].get("value") as HandlerParameter
                unitsDataset.forEach { if (it.get("name") == s.toString()) v = (it.get("value") as HandlerParameter) }
                (context as NewInterventionActivity).setQuantity(spinner.tag as Long, null, v, key, name, isGroup, groupId)
            }
        })
    }

    private fun setUnitSpinnerDataset(s: AutoCompleteTextView, product: InterItem) {
        val unitsDataset = arrayListOf<String>()
        val search = this.selectedUsage?.dose_unit!!.split("_")[0]
        this.handlers.forEach { if (it.unit!!.startsWith(search)){
            unitsDataset.add(Constants.getHandlerString(it, context))
        } }
        s.setText(if (product.getHandler() != null && !product.getHandler()!!.getIdentifier().isNullOrEmpty() && product.getHandler()!!.getIdentifier()?.toLowerCase() != "population") Constants.getHandlerString(HandlerParameter("", product.getHandler()!!.indicator, product.getHandler()!!.unit), context) else if(unitsDataset.isEmpty()) "" else unitsDataset[0])
        val arrayAdapter = ArrayAdapter(context, R.layout.item_spinner, unitsDataset)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        s.setAdapter(arrayAdapter)
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        arg0.setSelection(position)
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {}

    override fun getItemCount(): Int {
        return this.dataset.size
    }
}