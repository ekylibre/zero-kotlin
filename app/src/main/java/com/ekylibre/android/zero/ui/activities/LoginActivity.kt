package com.ekylibre.android.zero.ui.activities

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.zero.MainActivity
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.constants.NetworkConstants
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity: AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        applicationContext.getSharedPreferences(applicationContext.packageName, 0).registerOnSharedPreferenceChangeListener(
            this
        )

        server_adress_text_input_edit_text.setText(NetworkConstants.BASE_URL)
        username_text_input_edit_text.setText(NetworkConstants.TEST_USERNAME)
        password_text_input_edit_text.setText(NetworkConstants.TEST_PASSWORD)

        login_button.setOnClickListener {
            if (areTextFieldsValid()) {
                progressDialog = ProgressDialog(
                 this@LoginActivity,
                    R.style.AppTheme_Dialog
                 )
                progressDialog.setMessage("Authenticating...")
                progressDialog.show()

                val ekyAPIComSDK = EkyAPIComSDK(server_adress_text_input_edit_text.text.toString(), applicationContext)

                ekyAPIComSDK.setCredentials(
                    username_text_input_edit_text.text.toString(),
                    password_text_input_edit_text.text.toString()
                )
                ekyAPIComSDK.init()
            }
        }
    }

    private fun areTextFieldsValid(): Boolean {
        if (username_text_input_edit_text.text.toString() == "") return displayError(username_text_input_edit_text)
        if (password_text_input_edit_text.text.toString() == "") return displayError(password_text_input_edit_text)
        if (server_adress_text_input_edit_text.text.toString() == "") return displayError(server_adress_text_input_edit_text)
        return true
    }

    private fun displayError(editText: TextInputEditText): Boolean {
        (editText.parent.parent as TextInputLayout).error =
            com.ekylibre.android.zero.Utils.getDisplayString(
                "cannot_be_empty",
                applicationContext
            )
        return false
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        val value = Utils.readStringInSharedPreferences(key ?: "", applicationContext)
        if (key == "access_token") {
            if (value != "") {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                progressDialog.dismiss();
                finish()
            }
        }

        if (key == "authentication_error") {
            if (value != "") {
                progressDialog.dismiss()
                MaterialAlertDialogBuilder(this)
                    .setTitle("title")
                    .setMessage(value)
                    .setPositiveButton("OK") { _: DialogInterface, _: Int ->
                        Utils.saveInSharedPreferences(key, "", applicationContext)
                    }
                    .setCancelable(false)
                    .show()
            }
        }
    }
}