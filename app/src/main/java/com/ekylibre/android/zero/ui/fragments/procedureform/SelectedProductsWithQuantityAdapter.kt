package com.ekylibre.android.zero.ui.fragments.procedureform

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.Constants
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.HandlerParameter
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_selected_product_with_quantity.view.*
import javax.inject.Inject

class SelectedProductsWithQuantityAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<SelectedProductsWithQuantityAdapter.SelectedProductsWithQuantityViewHolder>() {

    class SelectedProductsWithQuantityViewHolder(val view: ConstraintLayout): RecyclerView.ViewHolder(view)

    private lateinit var name: String
    private lateinit var key: String
    private lateinit var dataset: ArrayList<InterItem>
    private var handlers: List<HandlerParameter> = listOf()
    private var isGroup: Boolean = false
    private var groupId: Int = 0

    fun setData(data: List<InterItem>, handlers: List<HandlerParameter>, key: String, name: String, isGroup: Boolean, groupId: Int) {
        this.dataset = data as ArrayList
        this.isGroup = isGroup
        this.groupId = groupId
        this.handlers = handlers

        this.key = key
        this.name = name
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectedProductsWithQuantityViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_selected_product_with_quantity, parent, false) as ConstraintLayout
        return SelectedProductsWithQuantityViewHolder(view)
    }

    override fun onBindViewHolder(holder: SelectedProductsWithQuantityViewHolder, position: Int) {
        holder.view.selected_product_name.text = dataset[position].getProductName()
        holder.view.selected_product_with_quantity_delete.setOnClickListener { (context as NewInterventionActivity).deleteSelectedProduct(key, name, dataset[position].getProductId(), this.isGroup, groupId) }

        initSpinner(holder.view.selected_product_unit_spinner, dataset[position])
        initQuantityEdit(holder.view.selected_product_quantity_edit, dataset[position])
    }

    private fun initQuantityEdit(et: EditText, product: InterItem) {
        et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as NewInterventionActivity).setQuantity(product.getProductId(), s.toString().replace(",", "."), null, key, name, isGroup, groupId)
            }
        })
        et.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                if (et.text.toString() == "0.0")
                    et.setText("")
            }
        }

        et.setText(product.getQuantity().toString() ?: "0.0")
    }

    private fun initSpinner(spinner: AutoCompleteTextView, product: InterItem) {
        spinner.tag = product.getProductId()

        val data: ArrayList<StringMap<String, Any>> = arrayListOf()
        this.handlers.forEach { data.add(StringMap.create(mapOf("name" to Constants.getHandlerString(it, context), "value" to it))) }

        spinner.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {
                var v = data[0].get("value") as HandlerParameter
                data.forEach { if (it.get("name") == s.toString()) v = (it.get("value") as HandlerParameter) }
                (context as NewInterventionActivity).setQuantity(spinner.tag as Long, null, v, key, name, isGroup, groupId)
            }
        })

        val datar = arrayListOf<String>()
        this.handlers.forEach { datar.add(Constants.getHandlerString(it, context)) }
        val arrayAdapter = ArrayAdapter(context, R.layout.item_spinner, datar)
        arrayAdapter.setDropDownViewResource(R.layout.item_dropdown_spinner)
        spinner.setAdapter(arrayAdapter)
        spinner.threshold = 100
        spinner.setText(if (product.getHandler() != null && !product.getHandler()!!.getIdentifier().isNullOrEmpty() && product.getHandler()!!.getIdentifier()?.toLowerCase() != "population") Constants.getHandlerString(HandlerParameter("", product.getHandler()!!.indicator, product.getHandler()!!.unit), context) else if(datar.isEmpty()) "" else datar[0])
    }

    override fun getItemCount(): Int {
        return this.dataset.size
    }
}