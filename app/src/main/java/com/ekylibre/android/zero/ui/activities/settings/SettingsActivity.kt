package com.ekylibre.android.zero.ui.activities.settings

import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.animation.AnimationUtils
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.client.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_settings.*

@AndroidEntryPoint
class SettingsActivity: AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
    private val model: SettingsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        close_settings_activity_btn.setOnClickListener { onBackPressed() }

        model.getCurrentSettings(applicationContext)
        initToggleListeners()
        save_settings_btn.setOnClickListener {
            model.save(planned_inter_switch.isChecked, record_inter_switch.isChecked, theme_inter_switch.isChecked, applicationContext)
            AppCompatDelegate.setDefaultNightMode(if (theme_inter_switch.isChecked) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
            delegate.applyDayNight()
            onBackPressed()
        }
        gps_btn.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        version_tv.text = model.version
        textView10.movementMethod = LinkMovementMethod.getInstance()
        textView11.movementMethod = LinkMovementMethod.getInstance()
        textView12.movementMethod = LinkMovementMethod.getInstance()
        textView13.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun initToggleListeners() {
        planned_inter_switch.isChecked = model.isPlannedSwitchChecked()
        planned_inter_switch.setOnCheckedChangeListener { buttonView, isChecked -> if (!record_inter_switch.isChecked && !isChecked) record_inter_switch.isChecked = true }

        record_inter_switch.isChecked = model.isRecordSwitchChecked()
        record_inter_switch.setOnCheckedChangeListener { buttonView, isChecked -> if (!isChecked && !planned_inter_switch.isChecked) planned_inter_switch.isChecked = true }

        theme_inter_switch.isChecked = model.isThemeSwitchChecked(this)

        sync_btn.setOnClickListener {
            model.serverDataSync()
            val colorInt = resources.getColor(R.color.ongoing_sync)
            sync_btn.imageTintList = ColorStateList.valueOf(colorInt)
            sync_btn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate))
        }
    }

    private fun setSuccessIcon() {
        val colorInt = resources.getColor(R.color.sync_success)
        sync_btn.imageTintList = ColorStateList.valueOf(colorInt)
        sync_btn.animation = null
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        model.onSharedPreferenceChanged(key)
        if (model.isSyncCompleteAndSuccessfull()) setSuccessIcon()
    }

    override fun onResume() {
        super.onResume()
        getSharedPreferences(packageName, 0)?.registerOnSharedPreferenceChangeListener(this)
    }
}