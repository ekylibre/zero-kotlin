package com.ekylibre.android.zero.ui.activities

import android.os.Bundle
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.zero.MainActivity
import com.ekylibre.android.zero.Utils


class SplashActivity : AppCompatActivity() {
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (EkyAPIComSDK.isUserLogged(applicationContext))
            Utils.startActivity(this, MainActivity::class.java)
        else Utils.startActivity(this, LoginActivity::class.java)

        finish()
    }
}