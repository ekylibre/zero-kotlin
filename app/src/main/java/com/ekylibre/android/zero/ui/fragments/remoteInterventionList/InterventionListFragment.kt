package com.ekylibre.android.zero.ui.fragments.remoteInterventionList

import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.AdapterView
import android.widget.SearchView
import android.widget.SimpleAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.SyncState
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionResponse
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.constants.AppConstants
import com.ekylibre.android.zero.ui.activities.settings.SettingsActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_remote_interventions.*
import kotlinx.android.synthetic.main.fragment_remote_interventions.view.*
import java.util.ArrayList
import javax.inject.Inject


@AndroidEntryPoint
open class InterventionListFragment: Fragment(), SharedPreferences.OnSharedPreferenceChangeListener {

    private var dataset = ArrayList<InterventionResponse>()
    private val model: InterventionListViewModel by viewModels()
    open var isAll = false

    @Inject
    lateinit var remoteInterventionListAdapter: RemoteInterventionListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_remote_interventions, container, false)

        if (!isAll) {
            model.filterState = AppConstants.REQUEST
            model.filterWorker = com.ekylibre.android.ekyapicom.sdk.Utils.readLongInSharedPreferences("eky_worker_id", requireContext()).toString()
        }
        initLiveData()
        initRecyclerView(root, R.id.remote_interventions_list_recycler_view, this.remoteInterventionListAdapter, LinearLayoutManager(context))

        return root
    }

    private fun initRecyclerView(view: View, recyclerId: Int, a: RecyclerView.Adapter<*>, manager: LinearLayoutManager) {
        view.findViewById<RecyclerView>(recyclerId).apply {
            setHasFixedSize(true)
            layoutManager = manager
            adapter = a
        }

        initSearchView(view)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //init state_spinner
        var data = arrayListOf(mapOf("name" to "Tous", "value" to ""),
            mapOf("name" to getString(R.string.local_inter), "value" to SyncState.LOCAL.ordinal.toString()),
            mapOf("name" to getString(R.string.planned_interventions), "value" to AppConstants.REQUEST),
            mapOf("name" to getString(R.string.in_progress_inter), "value" to AppConstants.IN_PROGRESS),
            mapOf("name" to getString(R.string.done_inter), "value" to AppConstants.RECORD),
            mapOf("name" to getString(R.string.validated_inter), "value" to AppConstants.VALIDATED)
        )
        state_spinner.adapter = SimpleAdapter(requireContext(), data, android.R.layout.simple_spinner_item, arrayOf("name"), intArrayOf(android.R.id.text1))
        state_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                model.filterState = (parent.getItemAtPosition(pos) as Map<String, String>).get("value") ?: ""
                model.fullInterventionsRefresh()
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
        state_spinner.setSelection(data.indexOf(data.find { it.get("value") == model.filterState }!!))

        //init worker_spinner
        data = arrayListOf(mapOf("name" to "Tous", "value" to ""))
        model.getWorkersFromDB().map { data.add(mapOf("name" to it.name, "value" to it.id.toString())) }
        worker_spinner.adapter = SimpleAdapter(requireContext(), data, android.R.layout.simple_spinner_item, arrayOf("name"), intArrayOf(android.R.id.text1))
        worker_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                model.filterWorker = (parent.getItemAtPosition(pos) as Map<String, String>).get("value") ?: ""
                model.fullInterventionsRefresh()
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
        worker_spinner.setSelection(data.indexOf(data.find { it.get("value") == model.filterWorker } ?: data[0]))

        filter_btn.setOnClickListener {
            val state = filters.visibility == View.VISIBLE
            val rotate = RotateAnimation(180f,0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
            rotate.duration = 150
            rotate.interpolator = LinearInterpolator()
            filter_btn.startAnimation(rotate)

            filter_btn.rotation = if(state) 0f else 180f
            filters.visibility = if(state) View.GONE else View.VISIBLE
        }
        swiperefresh.setOnRefreshListener { refresh() ; swiperefresh.isRefreshing = false }
    }

    override fun onResume() {
        super.onResume()
        context?.getSharedPreferences(context?.packageName, 0)
            ?.registerOnSharedPreferenceChangeListener(this)

        if (model.syncProcessed) model.offlineRefresh() else refresh()
    }

    private fun refresh() {
        if (Utils.isOnline(requireContext())) {
            model.serverDataSync()
            model.fullInterventionsRefresh()
        } else
            model.offlineRefresh()
    }

    private fun initLiveData() {
        val remoteInterventionsListObserver = Observer<List<InterventionResponse>> { newList -> onRemoteInterventionListChanged(newList) }
        model.interventionsListLiveData.observe(viewLifecycleOwner, remoteInterventionsListObserver)
    }

    private fun onRemoteInterventionListChanged(newList: List<InterventionResponse>) {
        this.dataset = newList as ArrayList<InterventionResponse>
        remoteInterventionListAdapter.setData(newList, model.proceduresByCategories, model.getStoredTargets())
    }

    private fun initSearchView(view: View) {
        view.remote_inter_searchview.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                remoteInterventionListAdapter.setData(ArrayList(dataset.filter { (it.name.uppercase()).contains(query.uppercase()) }))
                return false
            }
            override fun onQueryTextChange(newText: String): Boolean {
                remoteInterventionListAdapter.setData(ArrayList(dataset.filter { (it.name.uppercase()).contains(newText.uppercase()) }))
                return false
            }
        })
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        model.onSharedPreferenceChanged(key)
        if (key == sharedPreferences?.getString("username", null)+"_inter_to_sync") {
            refresh()
        }
    }
}