package com.ekylibre.android.zero.ui.fragments.procedureform.productlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PhytoUsage
import com.ekylibre.android.zero.R
import kotlinx.android.synthetic.main.item_phyto_usage.view.*
import javax.inject.Inject

class PhytoUsageListAdapter @Inject constructor(): RecyclerView.Adapter<PhytoUsageListAdapter.PhytoUsageListViewHolder>() {

    class PhytoUsageListViewHolder(val view: ConstraintLayout): RecyclerView.ViewHolder(view)

    private lateinit var dataset: ArrayList<PhytoUsage>

    fun setData(data: List<PhytoUsage>) {
        this.dataset = data as ArrayList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ):  PhytoUsageListViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_phyto_usage, parent, false) as ConstraintLayout
        return PhytoUsageListViewHolder(layout)
    }

    override fun onBindViewHolder(holder: PhytoUsageListViewHolder, position: Int) {
        holder.view.usage_info_max_dose.text = dataset[position].dose_quantity
        holder.view.usage_info_phd.text = (dataset[position].pre_harvest_delay / 3600).toString()
        holder.view.usage_info_znt_aquatics.text = (dataset[position].untreated_buffer_aquatic).toString() + "m"
        holder.view.usage_info_znt_plants.text = (dataset[position].untreated_buffer_plants).toString() + "m"
        holder.view.usage_info_znt_arthro.text = (dataset[position].untreated_buffer_arthropod).toString() + "m"
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}