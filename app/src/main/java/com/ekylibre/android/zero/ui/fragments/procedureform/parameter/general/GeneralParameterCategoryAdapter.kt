package com.ekylibre.android.zero.ui.fragments.procedureform.parameter.general

import android.content.Context
import com.ekylibre.android.zero.ui.fragments.procedureform.parameter.ParameterCategoryAdapter
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

class GeneralParameterCategoryAdapter @Inject constructor(@ActivityContext private val context: Context) : ParameterCategoryAdapter(context) {

    override var isGroup = false
}