package com.ekylibre.android.zero.ui.fragments.localinterventions

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_local_intervention.view.*
import kotlinx.android.synthetic.main.item_local_intervention.view.inter_name_tv
import kotlinx.android.synthetic.main.item_local_intervention.view.inter_start_date
import javax.inject.Inject
import kotlin.collections.ArrayList


class LocalInterventionAdapter @Inject constructor(@ActivityContext private val context: Context, var listener: OnItemClickListener): RecyclerView.Adapter<LocalInterventionAdapter.LocalInterventionListViewHolder>() {

    private var dataset: ArrayList<DisplayableIntervention> = ArrayList()

    inner class LocalInterventionListViewHolder(val view: ConstraintLayout): RecyclerView.ViewHolder(view)

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    fun setData(l: List<DisplayableIntervention>) {
        dataset.clear()
        dataset.addAll(l)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocalInterventionListViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_local_intervention, parent, false) as ConstraintLayout
        return LocalInterventionListViewHolder(constraintLayout)
    }

    override fun onBindViewHolder(holder: LocalInterventionListViewHolder, position: Int) {
        holder.view.inter_name_tv.text = Utils.getDisplayString(dataset[position].name, context)
        holder.view.inter_start_date.text = dataset[position].startDate
        dataset[position].preview?.let {
            holder.view.location_preview_tv.text = it
        }

        holder.view.delete_inter_btn.setOnClickListener {
            listener.onItemClick(position)
            dataset.removeAt(position)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}