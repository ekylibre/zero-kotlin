package com.ekylibre.android.zero.ui.fragments.accountList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.database.models.AccountDB
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.ui.fragments.AddAccountDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_accounts.*
import javax.inject.Inject

@AndroidEntryPoint
class AccountsFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager

    private val model: AccountsViewModel by viewModels()

    @Inject
    lateinit var viewAdapter: AccountsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_accounts, container, false)

        viewManager = LinearLayoutManager(context)

        context?.getSharedPreferences(context?.packageName, 0)
            ?.registerOnSharedPreferenceChangeListener(model)

        recyclerView = root.findViewById<RecyclerView>(R.id.accounts_list_recycler_view).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        setupObservers()

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        delete_current_account_btn.setOnClickListener { model.deleteCurrentAccount(requireContext()) }
        add_account_button.setOnClickListener {
            showAddAccountDialog()
        }
    }

    private fun showAddAccountDialog() {
        val dialog = AddAccountDialogFragment()
        dialog.show(parentFragmentManager, "AccountsFragment")
    }

    private fun setupObservers() {
        val currentAccountObserver = Observer<AccountDB> { newCurrentAccount ->
            current_account_api_url_tv.text = newCurrentAccount.api_url
            current_account_name_tv.text = newCurrentAccount.username
        }
        model.currentAccountLiveData.observe(viewLifecycleOwner, currentAccountObserver)

        val accountsListObserver = Observer<List<AccountDB>> { newList ->
            viewAdapter.setData(newList)
        }
        model.accountsListLiveData.observe(viewLifecycleOwner, accountsListObserver)
    }
}