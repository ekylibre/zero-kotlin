package com.ekylibre.android.zero.ui.activities.detailedintervention

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.caverock.androidsvg.SVG
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterWithProductDB
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.Parameter
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.ui.fragments.procedureform.parameter.ParameterAdapter
import com.google.android.material.button.MaterialButton
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_detailed_inter_param.view.*
import javax.inject.Inject

class DetailedInterventionParamAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<ParameterAdapter.ParameterListViewHolder>() {

    private lateinit var targetDetails: Map<Long, String>
    private var dataset: ArrayList<InterventionParameterWithProductDB> = ArrayList()
    private var key: String = ""
    private var theme = false

    fun setData(data: List<InterventionParameterWithProductDB>, key: String, targetsDetails: Map<Long, String>) {
        dataset.clear()
        dataset.addAll(data)
        this.key = key
        this.targetDetails = targetsDetails
        val username = com.ekylibre.android.ekyapicom.sdk.Utils.readStringInSharedPreferences("username", context)
        theme = com.ekylibre.android.ekyapicom.sdk.Utils.readBooleanInSharedPreferences(username + "_theme", context)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParameterAdapter.ParameterListViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_detailed_inter_param, parent, false) as ConstraintLayout
        return ParameterAdapter.ParameterListViewHolder(constraintLayout)
    }

    override fun onBindViewHolder(holder: ParameterAdapter.ParameterListViewHolder, position: Int) {
        holder.view.detailed_inter_name.text = dataset[position].interventionProduct?.getProductName()
        if ((dataset[position].interventionProduct?.getSvgShape() != null) && (dataset[position].interventionProduct?.getSvgShape() != "")) {
            var svgstr = dataset[position].interventionProduct!!.getSvgShape()
            if (theme) svgstr = svgstr!!.replace("stroke='black'", "stroke='white'")//Put right color according to theme
            holder.view.proc_param_icon.setImageDrawable(PictureDrawable(SVG.getFromString(svgstr).renderToPicture()))
        } else
            holder.view.proc_param_icon.setImageDrawable(Utils.getDisplayIcon(dataset[position].parameter.name ?: "", context))
        holder.view.detailed_inter_param_more.text = Utils.getDisplayString(dataset[position].parameter.name, holder.view.context)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}