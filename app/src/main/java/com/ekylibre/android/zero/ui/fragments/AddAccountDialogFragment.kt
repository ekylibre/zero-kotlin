package com.ekylibre.android.zero.ui.fragments

import android.app.Dialog
import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.zero.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.dialog_add_account.*
import kotlinx.android.synthetic.main.dialog_add_account.view.*
import kotlinx.android.synthetic.main.dialog_add_account.view.add_account_dialog_cancel_btn
import kotlinx.android.synthetic.main.dialog_add_account.view.add_account_dialog_save_btn

class AddAccountDialogFragment: DialogFragment(), SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var progressDialog: ProgressDialog

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val root = inflater.inflate(R.layout.dialog_add_account, null)

            context?.getSharedPreferences(context?.packageName, 0)
                ?.registerOnSharedPreferenceChangeListener(this)

            initButtons(root)

            builder.setView(root)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun initButtons(view: View) {
        view.add_account_dialog_save_btn.setOnClickListener {
            if (areTextFieldsValid(view)) {
                showProgressDialog()
                initEkyAPIComSDK(view)
                dialog?.cancel()
            }
        }
        view.add_account_dialog_cancel_btn.setOnClickListener { dialog?.cancel() }
    }

    private fun initEkyAPIComSDK(view: View) {
        val ekyAPIComSDK = EkyAPIComSDK(view.server_adress_dialog_edit_text.text.toString(), requireContext())
        ekyAPIComSDK.setCredentials(
            view.username_dialog_edit_text.text.toString(),
            view.password_dialog_edit_text.text.toString()
        )
        ekyAPIComSDK.init()
    }

    private fun areTextFieldsValid(view: View): Boolean {
        if (view.server_adress_dialog_edit_text.text.toString() == "") return displayError(view.server_adress_dialog_edit_text)
        if (view.username_dialog_edit_text.text.toString() == "") return displayError(view.username_dialog_edit_text)
        if (view.password_dialog_edit_text.text.toString() == "") return displayError(view.password_dialog_edit_text)
        return true
    }

    private fun displayError(editText: TextInputEditText): Boolean {
        (editText.parent.parent as TextInputLayout).error =
            com.ekylibre.android.zero.Utils.getDisplayString(
                "cannot_be_empty",
                requireContext()
            )
        return false
    }

    private fun showProgressDialog() {
        progressDialog = ProgressDialog(
            requireContext(),
            R.style.AppTheme_Dialog
        )
        progressDialog.setMessage(requireContext().getString(R.string.authenticating))
        progressDialog.show()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        try {
            if (key == "account_id")
                progressDialog.dismiss()
            if (key == "authentication_error")
                progressDialog.dismiss()
        } catch (e: UninitializedPropertyAccessException) {}
    }
}