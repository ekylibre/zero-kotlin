package com.ekylibre.android.zero.ui.fragments.procedureselection

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.constants.ProcedureFamilies
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_procedure_selection.*
import javax.inject.Inject

@AndroidEntryPoint
class ProcedureSelectionFragment : Fragment() {

    @Inject
    lateinit var viewAdapter: ProcedureCategoriesAdapter
    private val model: ProcedureSelectionViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_procedure_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        close_new_inter_activity_btn.setOnClickListener { requireActivity().finish() }
    }

    override fun onResume() {
        super.onResume()
        context?.let {
            val procedureFamilyId = (activity as NewInterventionActivity).getProcedureFamilyId()
            viewAdapter.setProcedureCategoriesData(ProcedureFamilies.procedureFamilies[procedureFamilyId])
            viewAdapter.setProceduresList(model.getAllProcedures())
        }
        initRecyclerView()
    }

    private fun initRecyclerView() {
        val viewManager = LinearLayoutManager(context)

        procedure_categories_recycler.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }
}