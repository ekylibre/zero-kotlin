package com.ekylibre.android.zero.ui.fragments.procedureform

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_simple_selected_product.view.*
import javax.inject.Inject

class SimpleSelectedProductsAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<SimpleSelectedProductsAdapter.SimpleSelectedProductsViewHolder>() {

    class SimpleSelectedProductsViewHolder(val view: ConstraintLayout): RecyclerView.ViewHolder(view)

    private var isGroup: Boolean = false
    private var groupId: Int = 0
    private lateinit var key: String
    private lateinit var name: String
    private lateinit var dataset: ArrayList<InterItem>

    fun setData(data: List<InterItem>, key: String, name: String, isGroup: Boolean, groupId: Int) {
        this.dataset = data as ArrayList
        this.key = key
        this.name = name
        this.isGroup = isGroup
        this.groupId = groupId
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleSelectedProductsViewHolder {
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_simple_selected_product, parent, false) as ConstraintLayout
        return SimpleSelectedProductsViewHolder(layout)
    }

    override fun onBindViewHolder(holder: SimpleSelectedProductsViewHolder, position: Int) {
        holder.view.selected_product_chip.text = dataset[position].getProductName()
        holder.view.selected_product_chip.setOnCloseIconClickListener { (context as NewInterventionActivity).deleteSelectedProduct(key, name, dataset[position].getProductId(), this.isGroup, this.groupId) }
    }

    override fun getItemCount(): Int {
        return this.dataset.size
    }

}
