package com.ekylibre.android.zero.ui.fragments.remoteInterventionList

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.Worker
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionResponse
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionToSend

class InterventionListViewModel @ViewModelInject constructor(private val ekyAPIComSDK: EkyAPIComSDK): ViewModel() {

    val interventionsListLiveData: MutableLiveData<List<InterventionResponse>> = MutableLiveData()
    val localInterventionsListLiveData: MutableLiveData<List<InterventionToSend>> = MutableLiveData()

    var proceduresByCategories: Map<String, ProcedureDB> =
        ekyAPIComSDK.getAllSimplifiedProcedures().associateBy(ProcedureDB::name)

    var syncProcessed = false
    private var syncResults = HashMap<String, Boolean>()

    var filterState: String = ""
    var filterWorker: String = ""

    fun getWorkersFromDB(): List<Worker> {
        return ekyAPIComSDK.getWorkersFromDB()
    }

    fun fullInterventionsRefresh() {
        //If some interventions have been pushed wait before pulling interventions because the API is not so quick or maybe cache issue
        if (pushLocalInterventions())
            Thread.sleep(500)
        refreshInterventionsFromAPI()
        refreshInterventionsFromDB()
    }

    fun pushLocalInterventions(): Boolean {
        return ekyAPIComSDK.pushLocalInterventions()
    }

    fun deleteLocalIntervention(id: Int) {
        localInterventionsListLiveData.value?.get(id)?.let { deleteLocalIntervention(it) }
    }

    fun deleteLocalIntervention(inter: InterventionToSend) {
        ekyAPIComSDK.deleteLocalIntervention(inter)
        refreshInterventionsFromDB()
    }

    fun offlineRefresh() {
        refreshInterventionsFromDB()
    }

    private fun refreshInterventionsFromAPI() {
        ekyAPIComSDK.getInterventionsFromAPI()
    }

    private fun refreshInterventionsFromDB() {
        refreshLocalInterventionsFromDB()

        var inters = ekyAPIComSDK.getRemoteInterventionsFromDB()
        if (!filterWorker.isNullOrEmpty() || !filterState.isNullOrEmpty())
            inters = inters.filter {
                var r1 = filterWorker == ""
                val r2 = filterState == "" || filterState == it.nature || filterState == it.syncState.toString() || filterState == it.state
                if (filterWorker != "")
                    it.parameters.forEach { if(it.product_id.toString() == filterWorker) r1 = true }
                r1 && r2
            }

        interventionsListLiveData.postValue(inters)
    }

    fun refreshLocalInterventionsFromDB() {
        localInterventionsListLiveData.postValue(ekyAPIComSDK.getLocalInterventionsFromDB().map { InterventionToSend.fromJson(it.jsonBody, it.id, it.error)!! })
    }

    fun getPreview(id: Long): String? {
        if (id >= 0)
            ekyAPIComSDK.getInterItemByIdFromDB(id)?.let { return it.getProductName() }
        return null
    }

    fun onSharedPreferenceChanged(key: String?) {
        if (key == "last_interventions_sync") {
            refreshInterventionsFromDB()
        } else
            key?.let {
                if (it.contains("last_") && it.contains("_sync"))
                    syncSuccess(it.substring(5, it.length -5))
            }
    }

    fun serverDataSync() {
        ekyAPIComSDK.apply {
            syncResults = hashMapOf("land_parcels" to false, "workers" to false, "equipments" to false, "plants" to false, "variants" to false, "matters" to false, "building_divisions" to false, "phytos" to false, "phytos_usages" to false, "animals" to false)
            getLandParcelsFromAPI()
            getVariantsFromAPI()
            getEquipmentsFromAPI()
            getPlantsFromAPI()
            getMattersFromAPI()
            getWorkersFromAPI()
            getBuildingDivisionsFromAPI()
            getPhytosFromAPI()
            getPhytoUsagesFromAPI()
            getAnimalsFromAPI()
        }
    }

    fun syncSuccess(syncKey: String) {
        this.syncResults[syncKey] = true

        Log.e("debugcount", this.syncResults.toString())
    }

    fun isSyncCompleteAndSuccessfull(): Boolean {
        return if (this.syncResults.values.all { it }) {
            this.syncResults = this.syncResults.mapValues { false } as HashMap<String, Boolean>
            this.syncProcessed = true
            true
        } else false
    }

    fun getStoredTargets(): List<InterItem> {
        val targetList = ArrayList<InterItem>()
        targetList.addAll(ekyAPIComSDK.getLandParcelsFromDB())
        targetList.addAll(ekyAPIComSDK.getPlantsFromDB())
        return targetList
    }
}