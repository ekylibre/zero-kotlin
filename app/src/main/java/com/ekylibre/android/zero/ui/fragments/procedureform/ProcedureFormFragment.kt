package com.ekylibre.android.zero.ui.fragments.procedureform

import android.app.Activity
import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionViewModel
import com.ekylibre.android.zero.ui.fragments.procedureform.datepicker.DatePickerFragment
import com.ekylibre.android.zero.ui.fragments.procedureform.datepicker.TimePickerFragment
import com.ekylibre.android.zero.ui.fragments.procedureselection.ProcedureSelectionFragmentDirections
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_procedure_form.*
import kotlinx.android.synthetic.main.fragment_procedure_form.close_new_inter_activity_btn
import kotlinx.android.synthetic.main.fragment_procedure_selection.*
import java.util.*


@AndroidEntryPoint
class ProcedureFormFragment: Fragment() {

    private val model: NewInterventionViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let { model.procedureName = it.getString("procedure_name").toString() }

        initObservers()
        model.prepare()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_procedure_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Load intervention
        arguments?.let { if (it.getLong("inter_id") != 0L) {
            val inter = model.loadIntervention(it.getLong("inter_id"))
            model.loadIntervention()

            val c = Calendar.getInstance()
            c.timeInMillis = inter.intervention.startDate
            tv_start_date.text = Utils.getDisplayDate(c)
            tv_start_time.text = Utils.getDisplayTime(c)

            val cc = Calendar.getInstance()
            cc.timeInMillis = inter.intervention.stopDate
            tv_end_date.text = Utils.getDisplayDate(cc)
            tv_end_time.text = Utils.getDisplayTime(cc)
        } }

        tv_inter_name.text = Utils.getDisplayString(model.procedureName, requireContext())
        close_new_inter_activity_btn.setOnClickListener { goBackToProcedureSelectionScreen() }
        save_btn.setOnClickListener {
            model.saveIntervention(false)
            requireActivity().finish()
        }
        initOnClickListeners()
    }

    private fun initObservers() {
        model.selectedStartDate.observe(this, {
            tv_start_date.text = Utils.getDisplayDate(it)
            tv_start_time.text = Utils.getDisplayTime(it)
        })
        model.selectedEndDate.observe(this, {
            tv_end_date.text = Utils.getDisplayDate(it)
            tv_end_time.text = Utils.getDisplayTime(it)
        })
        model.setDefaultDateAndTime()
    }

    private fun initOnClickListeners() {
        tv_start_date.setOnClickListener { DatePickerFragment(activity as NewInterventionActivity,  model.selectedStartDate.value!!).show(childFragmentManager, "startdatepicker") }
        tv_start_time.setOnClickListener { TimePickerFragment(activity as NewInterventionActivity,  model.selectedEndDate.value!!).show(childFragmentManager, "starttimepicker") }
        tv_end_date.setOnClickListener { DatePickerFragment(activity as NewInterventionActivity,  model.selectedStartDate.value!!).show(childFragmentManager, "enddatepicker") }
        tv_end_time.setOnClickListener { TimePickerFragment(activity as NewInterventionActivity,  model.selectedEndDate.value!!).show(childFragmentManager, "endtimepicker") }
    }

    private fun goBackToProcedureSelectionScreen() {
        val action = ProcedureFormFragmentDirections.actionProcedureFormFragmentToProcedureSelectionFragment()
        findNavController().navigate(action)
    }
}