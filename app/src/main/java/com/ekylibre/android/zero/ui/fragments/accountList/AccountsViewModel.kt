package com.ekylibre.android.zero.ui.fragments.accountList

import android.content.Context
import android.content.SharedPreferences
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.ekyapicom.sdk.database.models.AccountDB
import com.ekylibre.android.zero.MainActivity
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.ui.activities.LoginActivity
import com.ekylibre.android.zero.ui.activities.SplashActivity


class AccountsViewModel @ViewModelInject constructor(
    private val ekyAPIComSDK: EkyAPIComSDK) : ViewModel(), SharedPreferences.OnSharedPreferenceChangeListener {

    val accountsListLiveData: MutableLiveData<List<AccountDB>> = MutableLiveData()
    val currentAccountLiveData: MutableLiveData<AccountDB> = MutableLiveData()

    init {
        refreshAccounts()
    }

    fun refreshAccounts() {
        accountsListLiveData.postValue(ekyAPIComSDK.getInactiveAccountsFromDb())
        ekyAPIComSDK.getCurrentAccount()?.let { currentAccountLiveData.postValue(it) }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == "account_id") {
            refreshAccounts()
        }
    }

    fun deleteCurrentAccount(c: Context) {
        currentAccountLiveData.value?.let { ekyAPIComSDK.deleteAccount(it) }
        val l = ekyAPIComSDK.getInactiveAccountsFromDb()
        if (l.isNotEmpty())
            ekyAPIComSDK.switchCurrentAccount(l[0].a_id)
        else {
            Utils.startActivity(c, SplashActivity::class.java)
            ekyAPIComSDK.clearCurrentAccount()
        }
    }
}