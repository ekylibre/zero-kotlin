package com.ekylibre.android.zero.ui.activities.newintervention

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ekylibre.android.ekyapicom.sdk.Constants
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.ekyapicom.sdk.SyncState
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionWithParameterAndProductDB
import com.ekylibre.android.ekyapicom.sdk.filter.Onthology
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.*
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.*
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.Phyto
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PhytoUsage
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.GenericProcedureParameter
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.GenericProductParameter
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.HandlerParameter
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.Procedure
import com.ekylibre.android.zero.constants.AppConstants
import java.lang.NumberFormatException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class NewInterventionViewModel @ViewModelInject constructor(private val ekyAPIComSDK: EkyAPIComSDK): ViewModel() {

    val selectedStartDate: MutableLiveData<Calendar> by lazy {
        MutableLiveData<Calendar>()
    }

    val selectedEndDate: MutableLiveData<Calendar> by lazy {
        MutableLiveData<Calendar>()
    }

    var groupName: String = ""
    lateinit var procedureName: String
    lateinit var detailedProcedure: Procedure
    var generalSelectedProducts = HashMap<String, HashMap<String, MutableLiveData<ArrayList<InterItem>>>>()
    var groupSelectedProducts = ArrayList<HashMap<String, HashMap<String, MutableLiveData<ArrayList<InterItem>>>>>()
    var tmpSelectedProducts = ArrayList<InterItem>()

    var loadedProducts = ArrayList<InterItem>()
    var loadedUsages = HashMap<Long, List<PhytoUsage>>()

    lateinit var inter: InterventionWithParameterAndProductDB
    var groupModel: GenericProcedureParameter? = null

    fun saveSelectedProducts(key: String, name: String, isGroup: Boolean, groupId: Int) {
        if (isGroup)
                if (this.groupSelectedProducts[groupId][key] == null || this.groupSelectedProducts[groupId][key]!![name] == null)
                    TODO("Do something with this error.")
                else {
                    this.tmpSelectedProducts.map {
                        if (it is Matter && (it as Matter).france_maaid != null) {//Verify product has phyto with france_maaid
                            val phyto = ekyAPIComSDK.getPhytoByMaaidFromDB((it as Matter).france_maaid?.toLong() ?: 0L)//Get phyto product corresponding to france_maaid
                            if(phyto != null)this.loadedUsages[it.id] = ekyAPIComSDK.getPhytoUsagesFromDB(phyto.id)//Get Phyto usages with phyto product id
                        }
                    }
                    this.groupSelectedProducts[groupId][key]!![name]!!.value = ArrayList(this.tmpSelectedProducts)
                }
        else
            if (this.generalSelectedProducts[key] == null || this.generalSelectedProducts[key]!![name] == null)
                Log.e("ERROR", "ERROR")
            else {
                this.tmpSelectedProducts.map {
                    if (it is Matter && (it as Matter).france_maaid != null) {
                        val phyto = ekyAPIComSDK.getPhytoByMaaidFromDB((it as Matter).france_maaid?.toLong() ?: 0L)
                        if(phyto != null)this.loadedUsages[it.id] = ekyAPIComSDK.getPhytoUsagesFromDB(phyto.id)
                    }
                }
                this.generalSelectedProducts[key]!![name]!!.value = ArrayList(this.tmpSelectedProducts)
            }
    }

    fun prepare() {
        loadProcedureDetails()
    }

    fun loadIntervention(interId: Long): InterventionWithParameterAndProductDB {
        inter = ekyAPIComSDK.getRemoteInterventionFromDB(interId)
        this.procedureName = inter.intervention.procedure
        this.prepare()
        return inter
    }

    fun loadIntervention(): InterventionWithParameterAndProductDB {
        //Set stored dates values
        val c = Calendar.getInstance()
        c.timeInMillis = inter.intervention.startDate
        this.selectedStartDate.value = c
        val cc = Calendar.getInstance()
        cc.timeInMillis = inter.intervention.stopDate
        this.selectedEndDate.value = cc

        //Load products
        //inter.interventionParameters.map { if (it.interventionProduct != null) loadProductsList(it.interventionProduct!!.getProductName()) }

        //Load phyto usages
        inter.interventionParameters.map {
            if (it.interventionProduct is Matter && (it.interventionProduct as Matter).france_maaid != null) {
                val phyto = ekyAPIComSDK.getPhytoByMaaidFromDB((it.interventionProduct as Matter).france_maaid?.toLong() ?: 0L)
                if(phyto != null)this.loadedUsages[(it.interventionProduct as Matter).id] = ekyAPIComSDK.getPhytoUsagesFromDB(phyto.id)
            }
        }

        //Verify if this procedure have a group and initialize group products list
        val hasGroup = (detailedProcedure.parameters?.group?.size ?: 0) > 0
        if (hasGroup) groupSelectedProducts.add(hashMapOf())

        //set inputs values
        inter.interventionParameters.forEach {
            if (it.interventionProduct != null)
            //if the values are matching the zone(land_parcel, plant) group, put them in the group selection
                if (hasGroup && (it.parameter.name == "land_parcel" || it.parameter.name == "plant"))
                    this.groupSelectedProducts[0]
                        .getOrPut(it.parameter.role) { HashMap() }
                        .getOrPut(it.parameter.name) { MutableLiveData(ArrayList()) }
                        .value!!.add(it.interventionProduct!!)
                else
                    this.generalSelectedProducts
                        .getOrPut(it.parameter.role) { HashMap() }
                        .getOrPut(it.parameter.name) { MutableLiveData(ArrayList()) }
                        .value!!.add(it.interventionProduct!!)
        }

        return inter
    }

    private fun loadProcedureDetails() {
        this.generalSelectedProducts.clear()
        this.groupSelectedProducts.clear()
        this.detailedProcedure = ekyAPIComSDK.getDetailedProcedure(procedureName)
    }

    fun loadGroupProcedureParameters(): List<HashMap<String, ArrayList<GenericProductParameter>>> {
        if (groupSelectedProducts.size == 0)
            groupSelectedProducts.add(hashMapOf())

        this.groupName = detailedProcedure.parameters?.group?.get(0)?.name ?: ""
        groupModel = detailedProcedure.parameters?.group?.get(0)
        return detailedProcedure.parameters?.group?.map { constructMap(it) } ?: listOf()
    }

    fun addGroupItem(): List<HashMap<String, ArrayList<GenericProductParameter>>> {
        val cdetailedProcedure = detailedProcedure
        cdetailedProcedure.parameters?.group?.add(groupModel!!)
        detailedProcedure = cdetailedProcedure

        groupSelectedProducts.add(hashMapOf())

        return detailedProcedure.parameters?.group?.map { constructMap(it) }!!
    }

    fun removeGroupItem(position: Int): List<HashMap<String, ArrayList<GenericProductParameter>>> {
        val cdetailedProcedure = detailedProcedure
        cdetailedProcedure.parameters?.group?.removeAt(position)
        detailedProcedure = cdetailedProcedure

        groupSelectedProducts.removeAt(position)

        return detailedProcedure.parameters?.group?.map { constructMap(it) }!!
    }

    fun loadGeneralProcedureParameters(): HashMap<String, ArrayList<GenericProductParameter>> {
        val generic = GenericProcedureParameter("",
            detailedProcedure.parameters?.target, detailedProcedure.parameters?.output,
            detailedProcedure.parameters?.input, detailedProcedure.parameters?.doer,
            detailedProcedure.parameters?.tool)

        return constructMap(generic)
    }

    private fun constructMap(parameter: GenericProcedureParameter?): java.util.HashMap<String, java.util.ArrayList<GenericProductParameter>> {
        val map = HashMap<String, ArrayList<GenericProductParameter>>()
        parameter?.tool?.let { if (it.isNotEmpty()) (map.getOrPut(AppConstants.TOOL) { it as ArrayList }) }
        parameter?.doer?.let { if (it.isNotEmpty()) (map.getOrPut(AppConstants.DOER) { it as ArrayList }) }
        parameter?.input?.let { if (it.isNotEmpty()) (map.getOrPut(AppConstants.INPUT) { it as ArrayList }) }
        parameter?.output?.let { if (it.isNotEmpty()) (map.getOrPut(AppConstants.OUTPUT) { it as ArrayList }) }
        parameter?.target?.let { if (it.isNotEmpty()) (map.getOrPut(AppConstants.TARGET) { it as ArrayList }) }
        return map
    }

    fun setDefaultDateAndTime() {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        selectedStartDate.value = constructCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), hour, c.get(Calendar.MINUTE))
        selectedEndDate.value = constructCalendar(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), hour +1, c.get(Calendar.MINUTE))
    }

    fun updateSelectedProducts(id: Long, isGroup: Boolean) {
        if (isAlreadySelected(id))
            getSelectedProductIndexById(id)?.let { this.tmpSelectedProducts.removeAt(it) }
        else if (isGroup) {
            if (this.tmpSelectedProducts.size == 1)
                this.tmpSelectedProducts.clear()
            findProductById(id)?.let { this.tmpSelectedProducts.add(it) }
        } else
            findProductById(id)?.let { this.tmpSelectedProducts.add(it) }
    }

    private fun isAlreadySelected(id: Long): Boolean {
        tmpSelectedProducts.map { if (it.getProductId() == id) return true }
        return false
    }

    private fun getSelectedProductIndexById(id: Long): Int? {
        for (i in this.tmpSelectedProducts.indices)
            if (this.tmpSelectedProducts[i].getProductId() == id) return i

        return null
    }

    fun getLoadedUsages(): Map<Long, List<PhytoUsage>> {
        return this.loadedUsages
    }

    private fun findProductById(id: Long): InterItem? {
        loadedProducts.map {
            if (it.getProductId() == id)
                return it
        }
        return null
    }

    fun loadProductsList(key: String, name: String, filterString: String) {
        this.loadedProducts.clear()
        //Log.d("loadProductsList", "key=$key,name=$name,filter=$filterString recognised")

        when (key) {
            AppConstants.OUTPUT -> this.loadedProducts.addAll(ekyAPIComSDK.getVariantsFromDB())
            AppConstants.TARGET -> {
                if (name == "cultivation"){
                    if (filterString.startsWith("is plant"))
                        this.loadedProducts.addAll(ekyAPIComSDK.getPlantsFromDB())
                    else
                        this.loadedProducts.addAll(ekyAPIComSDK.getLandParcelsFromDB())
                } else {
                    if (filterString.startsWith("is animal") && !filterString.contains("animal_group"))
                        this.loadedProducts.addAll(ekyAPIComSDK.getAnimalsFromDB())
                    else if (filterString.startsWith("is land_parcel"))
                        this.loadedProducts.addAll(ekyAPIComSDK.getLandParcelsFromDB())
                    else if (filterString.startsWith("is plant") || name == "plant")
                        this.loadedProducts.addAll(ekyAPIComSDK.getPlantsFromDB())
                    else if (filterString.startsWith("is building_division"))
                        this.loadedProducts.addAll(ekyAPIComSDK.getBuildingDivisionsFromDB())
                    else if (filterString.startsWith("is animal_group"))
                        throw error("Procédures non implémentées")
                    else {
                        Log.d("loadProductsList", "key=$key,name=$name,filter=$filterString not recognised")
                        this.loadedProducts.addAll(ekyAPIComSDK.getAnimalsFromDB())
                        this.loadedProducts.addAll(ekyAPIComSDK.getLandParcelsFromDB())
                        this.loadedProducts.addAll(ekyAPIComSDK.getPlantsFromDB())
                        this.loadedProducts.addAll(ekyAPIComSDK.getBuildingDivisionsFromDB())
                    }
                }
            }
            AppConstants.INPUT -> this.loadedProducts.addAll(ekyAPIComSDK.getMattersFromDB())
            AppConstants.DOER -> this.loadedProducts.addAll(ekyAPIComSDK.getWorkersFromDB())
            AppConstants.TOOL -> this.loadedProducts.addAll(ekyAPIComSDK.getEquipmentsFromDB())
        }
    }

    fun getSelectedProducts(key: String, name: String, isGroup: Boolean, groupId: Int): ArrayList<InterItem> {
        this.tmpSelectedProducts = if (isGroup)
                ArrayList(this.groupSelectedProducts[groupId].getOrPut(key) { HashMap() }.getOrPut(name) { MutableLiveData(ArrayList()) }.value!!)
            else
                ArrayList(this.generalSelectedProducts.getOrPut(key) { HashMap() }.getOrPut(name) { MutableLiveData(ArrayList()) }.value!!)
        return this.tmpSelectedProducts
    }

    fun getSelectedProductsLiveData(key: String, name: String, isGroup: Boolean, groupId: Int): MutableLiveData<ArrayList<InterItem>> {
        return if (isGroup)
            this.groupSelectedProducts[groupId].getOrPut(key) { HashMap() }.getOrPut(name) { MutableLiveData(ArrayList()) }
        else
            this.generalSelectedProducts.getOrPut(key) { HashMap() }.getOrPut(name) { MutableLiveData(ArrayList()) }
    }

    private fun getHandlerIdentifier(unit: String?, key: String, name: String): String? {
        if (unit == null) return null
        if (key == AppConstants.INPUT)
            this.detailedProcedure.parameters?.input?.map { if (it.name == name) return HandlerParameter.toUnitMap(it.handler)[unit]}
        if (key == AppConstants.OUTPUT)
            this.detailedProcedure.parameters?.output?.map { if (it.name == name) return HandlerParameter.toUnitMap(it.handler)[unit]}
        return null
    }

    fun setQuantity(productId: Long, quantity: String?, handler: HandlerParameter?, key: String, name: String, isGroup: Boolean, groupId: Int) {
        val selectedProductsLiveData = if (isGroup)
            this.groupSelectedProducts[groupId][key]?.get(name)
        else
            this.generalSelectedProducts[key]?.get(name)

        selectedProductsLiveData?.value?.map { i ->
            try {
                if (i.getProductId() == productId) {
                    if (name == AppConstants.CHILD)
                        i.setQuantity(quantity?.toDouble(), handler)
                    else
                        i.setQuantity(quantity?.toDouble(), handler)
                }
            } catch (e: NumberFormatException) {}
        }
    }

    fun setNewBornDetails(productId: Long, newBornName: String?, idNumber: Long?, sex: String?, birthCdt: String?, healthy: Boolean?, key: String, name: String, isGroup: Boolean, groupId: Int) {
        val selectedProductsLiveData = if (isGroup)
            this.groupSelectedProducts[groupId][key]?.get(name)
        else
            this.generalSelectedProducts[key]?.get(name)

        selectedProductsLiveData?.value?.map { v ->
            if (v.getProductId() == productId) {
                try {
                    (v as Variant).setNewbornInfos(newBornName, idNumber, sex, birthCdt, healthy)
                } catch (e: NumberFormatException) {}
            }
        }
    }

    fun deleteSelectedProduct(key: String, name: String, productId: Long, isGroup: Boolean, groupId: Int) {
        val selectedProductsLiveData =  if (isGroup)
            this.groupSelectedProducts[groupId][key]?.get(name)
        else
            this.generalSelectedProducts[key]?.get(name)

        val newList = ArrayList<InterItem>()
        selectedProductsLiveData?.value?.let { newList.addAll(it) }
        newList.removeAll { it.getProductId() == productId }

        selectedProductsLiveData?.value = newList
    }

    fun saveIntervention(confirm: Boolean) {
        val update = this::inter.isInitialized && this.inter.intervention.syncState == SyncState.REMOTE.ordinal && !confirm

        //Save InterventionToSend
        val p = Providers(ProvidersData(0))
        var w = listOf(WorkingPeriodsAttributes.fromCalendar(this.selectedStartDate.value, this.selectedEndDate.value))
        var d = getParameters(AppConstants.DOER, this.generalSelectedProducts)
        var i = getParameters(AppConstants.INPUT, this.generalSelectedProducts)
        var o = getParameters(AppConstants.OUTPUT, this.generalSelectedProducts)
        var ta = getParameters(AppConstants.TARGET, this.generalSelectedProducts)
        var to = getParameters(AppConstants.TOOL, this.generalSelectedProducts)
        val g = if (this.groupName.isBlank())
            null
        else listOf(GroupParamAttributes(null, this.groupName, getGroupParameters(AppConstants.TARGET, this.groupSelectedProducts), getGroupParameters(AppConstants.OUTPUT, this.groupSelectedProducts),
            getGroupParameters(AppConstants.INPUT, this.groupSelectedProducts), getGroupParameters(AppConstants.TOOL, this.groupSelectedProducts), getGroupParameters(AppConstants.DOER, this.groupSelectedProducts), null))

        if (update) {
            val editedinter = this::inter.get()
            //Update working period
            if(editedinter.intervention.workingPeriodId != -1L) w.get(0).id = editedinter.intervention.workingPeriodId
            //Destroy old parameters
            editedinter.interventionParameters.forEach {
                val par = ParamAttributes(it.parameter.p_id, null, null, "", null, null, null, null, null, null, null, "1", null)
                when (it.parameter.role) {
                    AppConstants.DOER -> d = d?.plus(par)
                    AppConstants.INPUT -> i = i?.plus(par)
                    AppConstants.OUTPUT -> o = o?.plus(par)
                    AppConstants.TARGET -> ta = ta?.plus(par)
                    AppConstants.TOOL -> to = to?.plus(par)
                }
            }
            //TODO do the same for group_parameters
        }

        val inter = InterventionToSend(p, this.procedureName, w, d, i, o, ta, to, g)

        //Save Intervention
        val params = arrayListOf<Parameter>()
        this.generalSelectedProducts.forEach { r -> r.value.forEach { p ->
            p.value.value?.forEach {
                val id = System.currentTimeMillis()
                params.add(Parameter(id, 0, r.key, p.key, it.getProductName(), it.getProductId(), it.getHandler()?.unit, it.getHandler()?.indicator, it.getQuantity(), if (it is Matter) it.getUsageId() else null))
                Thread.sleep(1)//wait to have different id
            }
        } }
        this.groupSelectedProducts.forEachIndexed { index, r -> r.forEach { p ->
            p.value.forEach { u -> u.value.value?.forEach {
                val id = System.currentTimeMillis()
                params.add(Parameter(id, 0, p.key, u.key, it.getProductName(), it.getProductId(), it.getHandler()?.unit, it.getHandler()?.indicator, it.getQuantity(), if (it is Matter) it.getUsageId() else null))
                Thread.sleep(1)//wait to have different id
            } }
        } }

        val id = if(this::inter.isInitialized && (this.inter.intervention.syncState == SyncState.LOCAL.ordinal || !confirm)) this.inter.intervention.i_id else System.currentTimeMillis()
        val intr = InterventionResponse(id, "record", this.procedureName, "0", "",
            this.selectedStartDate.value?.time ?: Date(), this.selectedEndDate.value?.time ?: Date(),
            "", "done", 0, 0, Costing(.0, .0, .0, .0,.0), mutableListOf(WorkingPeriodsAttributes(-1L, "", "", null)), params,
            if (update) SyncState.REMOTE.ordinal else SyncState.LOCAL.ordinal)
        ekyAPIComSDK.storeIntervention(inter, intr)
    }

    private fun getGroupParameters(s: String, selectedProducts: ArrayList<HashMap<String, HashMap<String, MutableLiveData<ArrayList<InterItem>>>>>): List<ParamAttributes>? {
        val res = ArrayList<ParamAttributes>()
        selectedProducts.forEach {
            it[s]?.map { namesMap ->
                val tmp = namesMap.value.value

                tmp?.map {
                    if ((s == AppConstants.OUTPUT) && (it is Variant))
                        if (it.getNewBornName() != "")
                            res.add(ParamAttributes(null, null, it.getProductId(), namesMap.key, null, null, null, 1, it.getNewBornName(), it.getNewBornID(), null, null, constructReadingAttributes(it)))
                        else
                            res.add(ParamAttributes(null, null, it.getProductId(), namesMap.key, null, if (it.variety == "plant") null else it.getQuantity()/*TODO culture(plant) is a variant, there is no need to put quantities and units*/, if (it.variety == "plant") null else if(it.getHandler() != null) it.getHandler()!!.getIdentifier() else null, null, null, null, null, null, null))
                    else
                        res.add(ParamAttributes(null, it.getProductId(), null, namesMap.key, null, it.getQuantity(), if(it.getHandler() != null) it.getHandler()!!.getIdentifier() else null, null, null, null, if (it is Matter) it.getUsageId() else null, null, null))
                }
            }
        }
            return if (res.size > 0)
                res
            else
                null
    }

    private fun getParameters(s: String, selectedProducts: HashMap<String, HashMap<String, MutableLiveData<ArrayList<InterItem>>>>): List<ParamAttributes>? {
        val res = ArrayList<ParamAttributes>()
        selectedProducts[s]?.map { namesMap ->
            val tmp = namesMap.value.value

            tmp?.map {
                //val readingAttributes = ReadingAttribute.constructForAPI(Constants.OUTPUT_ATTR_HOUR_COUNTER, it.getHours(), "hour")
                if ((s == AppConstants.OUTPUT) && (it is Variant))
                    if (it.getNewBornName() != "")
                        res.add(ParamAttributes(null, null, it.getProductId(), namesMap.key, null, null, null, 1, it.getNewBornName(), it.getNewBornID(), null, null, constructReadingAttributes(it)))
                    else
                        res.add(ParamAttributes(null, null, it.getProductId(), namesMap.key, null, if (it.variety == "plant") null else it.getQuantity(), if (it.variety == "plant") null else if(it.getHandler() != null) it.getHandler()!!.getIdentifier() else null, null, null, null, null, null, null))
                else
                    res.add(ParamAttributes(null, it.getProductId(), null, namesMap.key, null, it.getQuantity(), if(it.getHandler() != null) it.getHandler()!!.getIdentifier() else null, 1, null, null, if (it is Matter) it.getUsageId() else null, null, null))
            }
        }
        return if (res.size > 0)
            res
        else
            null
    }

    private fun constructReadingAttributes(it: Variant): List<ReadingAttribute>? {
        val res = ArrayList<ReadingAttribute>()
        ReadingAttribute.constructForAPI(Constants.OUTPUT_ATTR_NET_MASS, it.getQuantity(), it.getHandler()?.unit?:"")?.let { res.add(it) }
        ReadingAttribute.constructForAPI(Constants.OUTPUT_ATTR_SEX, it.getNewBornGender())?.let { res.add(it) }
        ReadingAttribute.constructForAPI(Constants.OUTPUT_ATTR_MAMMALIA_BIRTH_CDT, it.getNewBornBirthCondition())?.let { res.add(it) }
        ReadingAttribute.constructForAPI(Constants.OUTPUT_ATTR_HEALTHY, it.isNewBornHealthy())?.let { res.add(it) }

        return res
    }

    fun updateDate(tag: String?, hour: Int, minute: Int) {
        when (tag) {
            "starttimepicker" -> {
                this.selectedStartDate.value = constructCalendar(selectedStartDate.value, hour, minute)
                updateDate("endtimepicker", this.selectedStartDate.value!!.get(Calendar.HOUR_OF_DAY) + 1, this.selectedStartDate.value!!.get(Calendar.MINUTE))
            }
            "endtimepicker" -> this.selectedEndDate.value = constructCalendar(selectedStartDate.value, hour, minute)
        }
    }

    fun updateDate(tag: String?, year: Int, month: Int, day: Int) {
        when (tag) {
            "startdatepicker" -> {
                this.selectedStartDate.value = constructCalendar(selectedStartDate.value, year, month, day)
                updateDate("enddatepicker", year, month, day)
                updateDate("endtimepicker", this.selectedStartDate.value!!.get(Calendar.HOUR_OF_DAY) + 1, this.selectedStartDate.value!!.get(Calendar.MINUTE))
            }
            "enddatepicker" -> this.selectedEndDate.value = constructCalendar(selectedStartDate.value, year, month, day)
        }
    }

    private fun constructCalendar(selectedDate: Calendar?, hour: Int, minute: Int): Calendar {
        if (selectedDate != null)
            return constructCalendar(selectedDate.get(Calendar.YEAR), selectedDate.get(Calendar.MONTH), selectedDate.get(Calendar.DAY_OF_MONTH), hour, minute)
        return Calendar.getInstance()
    }

    private fun constructCalendar(selectedDate: Calendar?, year: Int, month: Int, day: Int): Calendar {
        if (selectedDate != null)
            return constructCalendar(year, month, day, selectedDate.get(Calendar.HOUR_OF_DAY), selectedDate.get(Calendar.MINUTE))
        return Calendar.getInstance()
    }

    private fun constructCalendar(year: Int, month: Int, day: Int, hour: Int, minute: Int): Calendar {
        val c = Calendar.getInstance()
        c.set(year, month, day, hour, minute)
        return c
    }

    fun getCurrentUserId(): Long {
        return ekyAPIComSDK.getCurrentWorkerId()
    }

    fun addDefaultItem(id: Long) {
        if (!isAlreadySelected(id)) findProductById(id)?.let { this.tmpSelectedProducts.add(it) }
    }
}