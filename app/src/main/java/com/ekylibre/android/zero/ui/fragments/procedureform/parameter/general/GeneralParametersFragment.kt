package com.ekylibre.android.zero.ui.fragments.procedureform.parameter.general

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.ui.fragments.procedureform.parameter.ParametersFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class GeneralParametersFragment: ParametersFragment() {

    @Inject
    lateinit var viewAdapter: GeneralParameterCategoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root =  inflater.inflate(R.layout.fragment_procedure_parameters, container, false)
        initAdapter()
        initRecyclerView(root, viewAdapter)
        return root
    }

    override fun initAdapter() {
        viewAdapter.setData(model.loadGeneralProcedureParameters(), false, 0)
    }
}