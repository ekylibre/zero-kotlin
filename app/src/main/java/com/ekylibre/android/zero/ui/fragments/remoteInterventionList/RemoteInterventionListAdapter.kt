package com.ekylibre.android.zero.ui.fragments.remoteInterventionList

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.SyncState
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionResponse
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.constants.AppConstants
import com.ekylibre.android.zero.constants.ProcedureCategories
import com.ekylibre.android.zero.ui.activities.detailedintervention.DetailedInterventionActivity
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_remote_intervention.view.*
import kotlinx.android.synthetic.main.item_remote_intervention.view.inter_name_tv
import kotlinx.android.synthetic.main.item_remote_intervention.view.inter_start_date
import javax.inject.Inject
import kotlin.math.round

class RemoteInterventionListAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<RemoteInterventionListAdapter.InterventionListViewHolder>() {

    private var interDataset: ArrayList<InterventionResponse> = ArrayList()
    private var proceduresByCategories: Map<String, ProcedureDB>? = null
    private lateinit var storedTargets: List<InterItem>

    class InterventionListViewHolder(val view: ConstraintLayout) : RecyclerView.ViewHolder(view)

    fun setData(newData: List<InterventionResponse>, proceduresByCategories: Map<String, ProcedureDB>, storedTargets: List<InterItem>) {
        setData(newData)
        this.storedTargets = storedTargets
        if (this.proceduresByCategories.isNullOrEmpty()) this.proceduresByCategories = proceduresByCategories
    }

    fun setData(newData: List<InterventionResponse>) {
        this.interDataset.clear()
        this.interDataset.addAll(newData.sortedByDescending { it.started_at })

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterventionListViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_remote_intervention, parent, false) as ConstraintLayout
        return InterventionListViewHolder(constraintLayout)
    }

    override fun onBindViewHolder(holder: InterventionListViewHolder, position: Int) {
        setDividerColor(holder.view, position)

        val currentInterId = interDataset[position].id
        holder.view.setOnClickListener { Utils.startActivity(context, DetailedInterventionActivity::class.java, "inter_id", currentInterId) }

        holder.view.inter_name_tv.text = Utils.getDisplayString(interDataset[position].procedure_name, context) + " " + context.getString(R.string.number) + interDataset[position].number
        holder.view.inter_start_date.text = Utils.getDisplayTime(interDataset[position].started_at)
        holder.view.inter_stop_date_tv.text = Utils.getDisplayTime(interDataset[position].stopped_at)
        holder.view.inter_start_day.text = Utils.getDisplayDate(interDataset[position].started_at)

        val inputs = interDataset[position].parameters.filter { it.role == AppConstants.INPUT || it.role == AppConstants.TOOL }
        var names = ""
        inputs.forEach { names += getParameterName(it.product_id) + " " }
        holder.view.inter_des_tv.text = names

        (interDataset[position].getTargetsQuantity().toString() + " " + context.getString(R.string.target) + "(s) • " + getWorkArea(position).toString() + " " + context.getString(R.string.hectare)).also { holder.view.local_inter_preview.text = it }

        if (interDataset[position].syncState == SyncState.LOCAL.ordinal)
            holder.view.inter_nature.setImageDrawable(this.context.getDrawable(R.drawable.ic_sync))
        else if (interDataset[position].state == AppConstants.VALIDATED)
            holder.view.inter_nature.setImageDrawable(this.context.getDrawable(R.drawable.ic_done))
        else if (interDataset[position].state == AppConstants.IN_PROGRESS)
            holder.view.inter_nature.setImageDrawable(this.context.getDrawable(R.drawable.ic_work_process))
        else if (interDataset[position].nature == AppConstants.RECORD)
            holder.view.inter_nature.setImageDrawable(this.context.getDrawable(R.drawable.ic_check))
        else
            holder.view.inter_nature.setImageDrawable(this.context.getDrawable(R.drawable.ic_calendar))
    }

    private fun getWorkArea(position: Int): Double {
        val ids = interDataset[position].getTargets().split(", ").map { if (it == "") 0 else it.toLong() }
        val surface = this.storedTargets.filter { ids.contains(it.getProductId()) }.sumOf { (it.getSurfaceArea()?.value ?: "0").toDouble() }
        return round(surface * 100) / 100
    }

    private fun getParameterName(id: Long): String {
        return this.storedTargets.singleOrNull { id == it.getProductId() }?.getProductName() ?: ""
    }

    private fun setDividerColor(view: View, position: Int) {
        val c = this.proceduresByCategories?.get(interDataset[position].procedure_name)?.categories ?: ""
        val color = ContextCompat.getColor(context, ProcedureCategories.getByCategoryName(c.split(",")[0])?.colorId ?: R.color.design_default_color_background)
        view.inter_type_divider.background?.setColorFilter(color, android.graphics.PorterDuff.Mode.SRC_IN)
    }

    private fun setLayoutColor(v: View?, color: Int) {
        v?.let { (it as ImageView).setColorFilter(color, android.graphics.PorterDuff.Mode.SRC_IN) }
    }

    override fun getItemCount(): Int {
        return interDataset.size
    }
}
