package com.ekylibre.android.zero.ui.activities.detailedintervention

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterWithProductDB
import com.ekylibre.android.zero.R
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_parameter_category.view.*
import javax.inject.Inject

class DetailedInterventionParamCategoryAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<DetailedInterventionParamCategoryAdapter.ParameterCategoriesListViewHolder>() {

    class ParameterCategoriesListViewHolder(val view: ConstraintLayout) : RecyclerView.ViewHolder(view)

    private var dataset: ArrayList<String> = arrayListOf("target", "input", "output", "tool", "doer")
    private lateinit var paramsMap: Map<String, List<InterventionParameterWithProductDB>>
    private lateinit var targetsDetails: Map<Long, String>

    fun setData(data: Map<String, List<InterventionParameterWithProductDB>>, targetsDetails: Map<Long, String>) {
        val tmp = ArrayList(dataset)
        tmp.map { if (!data.keys.contains(it)) dataset.remove(it) }
        this.paramsMap = data
        this.targetsDetails = targetsDetails
        notifyDataSetChanged()
    }

    private fun initRecyclerView(procedureListRecycler: RecyclerView, data: List<InterventionParameterWithProductDB>, key: String) {
        val viewManager = LinearLayoutManager(context)

        val paramListAdapter = DetailedInterventionParamAdapter(context)
        paramListAdapter.setData(data, key, this.targetsDetails)
        procedureListRecycler.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = paramListAdapter
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParameterCategoriesListViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_detailed_inter_parameter_category, parent, false) as ConstraintLayout
        return ParameterCategoriesListViewHolder(constraintLayout)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(holder: ParameterCategoriesListViewHolder, position: Int) {
        this.paramsMap[dataset[position]]?.let {
            initRecyclerView(
                holder.view.dialog_products_recycler_view,
                it,
                dataset[position]
            )
        }
    }
}