package com.ekylibre.android.zero.ui.fragments.procedureform.productlist

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.Phyto
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PhytoUsage

class PhytoInfoViewModel @ViewModelInject constructor(private val ekyAPIComSDK: EkyAPIComSDK): ViewModel()  {

    fun getPhytoInfo(phytoId: Long): Phyto? {
        return ekyAPIComSDK.getPhytoFromDB(phytoId)
    }

    fun getPhytoUsage(phytoId: Long): List<PhytoUsage> {
        return ekyAPIComSDK.getPhytoUsagesFromDB(phytoId)
    }
}