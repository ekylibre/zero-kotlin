package com.ekylibre.android.zero.ui.fragments.procedureform.productlist

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekylibre.android.ekyapicom.sdk.filter.Filter
import com.ekylibre.android.ekyapicom.sdk.filter.Onthology
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.constants.AppConstants
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dialog_product_selector.*
import kotlinx.android.synthetic.main.dialog_product_selector.view.*
import java.util.*

@AndroidEntryPoint
class ProductListDialogFragment: DialogFragment(), ProductListAdapter.OnItemClickListener {

    private lateinit var productAdapter: ProductListAdapter
    private lateinit var productAdapterDataset: ArrayList<InterItem>
    private var key = ""
    private var filterString = ""
    private var name = ""
    private var isGroup: Boolean = false
    private var groupId: Int = 0
    private val model: NewInterventionViewModel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let { fragmentActivity ->
            val builder = AlertDialog.Builder(fragmentActivity)
            val inflater = requireActivity().layoutInflater
            val root = inflater.inflate(R.layout.dialog_product_selector, null)

            requireArguments().getString("key")?.let {
                if (it.contains("@group")) {
                    this.isGroup = true
                    this.key = it.split("@group")[0]
                } else
                    this.key = it
            }

            requireArguments().getString("filter")?.let { this.filterString = it }
            requireArguments().getString("name")?.let { this.name = it }
            requireArguments().getInt("groupId").let { this.groupId = it }

            initAdapter(root, LinearLayoutManager(context))
            initSearchView(root)
            initButtons(root)

            builder.setView(root)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun initButtons(view: View) {
        view.add_account_dialog_save_btn.setOnClickListener { model.saveSelectedProducts(key, name, isGroup, groupId) ; dialog?.cancel() }
        view.add_account_dialog_cancel_btn.setOnClickListener { dialog?.cancel() }
    }

    private fun initAdapter(view: View, viewManager: LinearLayoutManager) {
        model.loadProductsList(this.key, this.name, this.filterString)
        this.productAdapter = ProductListAdapter(model.getCurrentUserId(), this@ProductListDialogFragment, isGroup)
        this.productAdapterDataset = Filter.filterList(model.loadedProducts, this.filterString, model.selectedStartDate.value, model.selectedEndDate.value)
        productAdapter.setData(this.productAdapterDataset, model.getSelectedProducts(key, name, isGroup, groupId))
        view.dialog_products_recycler_view.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = productAdapter
        }
    }

    private fun initSearchView(view: View) {
        view.product_list_searchview.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                productAdapter.setData(ArrayList(productAdapterDataset.filter { (it.getProductName().uppercase()).contains(query.uppercase()) }), model.getSelectedProducts(key, name, isGroup, groupId))
                return false
            }
            override fun onQueryTextChange(newText: String): Boolean {
                productAdapter.setData(ArrayList(productAdapterDataset.filter { (it.getProductName().uppercase()).contains(newText.uppercase()) }), model.getSelectedProducts(key, name, isGroup, groupId))
                return false
            }
        })

        /*view.product_list_searchview.isIconifiedByDefault = true
        view.product_list_searchview.isFocusable = true
        view.product_list_searchview.isIconified = false
        view.product_list_searchview.clearFocus()
        view.product_list_searchview.requestFocusFromTouch()*/
    }

    override fun onItemClick(id: Long) {
        model.updateSelectedProducts(id, isGroup)
        //autosave inputs
        if (this.key == AppConstants.INPUT) {
            model.saveSelectedProducts(key, name, isGroup, groupId)
            dialog?.cancel()
        }
    }

    override fun onDefaultItemSet(id: Long) {
        model.addDefaultItem(id)
    }

    companion object {
        fun newInstance(key: String, filter: String?, name: String?, groupId: Int?): ProductListDialogFragment {
            return ProductListDialogFragment().apply {
                arguments = bundleOf("key" to key, "filter" to filter, "name" to name, "groupId" to groupId)
            }
        }
    }
}