package com.ekylibre.android.zero.ui.fragments.procedureform.productlist

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.caverock.androidsvg.SVG
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.Matter
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.Worker
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.Phyto
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import kotlinx.android.synthetic.main.item_product.view.*
import javax.inject.Inject


class ProductListAdapter @Inject constructor(private val userId: Long, var listener: OnItemClickListener, private var isGroup: Boolean): RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder>() {

    private var dataset: ArrayList<InterItem> = ArrayList()
    private var selectedProducts: ArrayList<InterItem> = ArrayList()
    private var tmpSelectedProducts: ArrayList<InterItem> = ArrayList()
    private lateinit var context: Context
    private var holders: Array<ProductListViewHolder?> = arrayOf()

    inner class ProductListViewHolder(val view: ConstraintLayout): RecyclerView.ViewHolder(view)

    interface OnItemClickListener {
        fun onItemClick(id: Long)
        fun onDefaultItemSet(id: Long)
    }

    fun setData(d: ArrayList<InterItem>, p: List<InterItem>) {
        dataset.clear()
        d.sortBy { it.getProductName() }
        dataset.addAll(d)
        holders = Array(dataset.size) { i -> null }
        selectedProducts.clear()
        selectedProducts.addAll(p)
        tmpSelectedProducts.clear()
        tmpSelectedProducts.addAll(p)

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListViewHolder {
        this.context = parent.context
        val constraintLayout = LayoutInflater.from(this.context)
            .inflate(R.layout.item_product, parent, false) as ConstraintLayout
        return ProductListViewHolder(constraintLayout)
    }

    override fun onBindViewHolder(holder: ProductListViewHolder, position: Int) {
        this.holders.set(position, holder)
        val interItem = dataset[position]
        holder.view.product_btn.text = if (interItem is Matter)
            Html.fromHtml(interItem.name + "<br/>" + "<small>" + interItem.population + " " + interItem.unit_name + " • " + interItem.container_name + "</small>")
        else interItem.getProductName()
        selectedProducts.map { if (it.getProductId() == interItem.getProductId()) setDividerColor(holder.view, true) }

        //By default, the current user is checked in the workers list.
        if (interItem is Worker)
            if (interItem.getProductId() == this.userId) {
                setDividerColor(holder.view, true)
                listener.onDefaultItemSet(interItem.getProductId())
                this.tmpSelectedProducts.add(interItem)
            }

        holder.view.product_btn.setOnClickListener { onClickedProduct(holder, position) }

        setItemPicto(position, holder)
    }

    private fun onClickedProduct(holder: ProductListViewHolder, position: Int) {
        val wasProductAlreadySelected = this.tmpSelectedProducts.contains(dataset[position])
        if (wasProductAlreadySelected)
            this.tmpSelectedProducts.remove(dataset[position])
        else if (isGroup) {
            if (this.tmpSelectedProducts.size == 1) {
                var pos = -1
                dataset.map { if (it.getProductId() == this.tmpSelectedProducts[0].getProductId()) pos = dataset.indexOf(it) }
                setDividerColor(this.holders[pos]!!.view, false)
                this.tmpSelectedProducts.clear()
            }
            this.tmpSelectedProducts.add(dataset[position])
        } else
            this.tmpSelectedProducts.add(dataset[position])

        setDividerColor(holder.view, !wasProductAlreadySelected)
        listener.onItemClick(dataset[position].getProductId())
    }

    private fun setDividerColor(view: View, toCheck: Boolean) {
        val background = if (toCheck)
            R.drawable.selected_background
        else
            R.drawable.unselected_background
        view.setBackgroundResource(background)
    }

    private fun setItemPicto(position: Int, holder: ProductListViewHolder) {
        when {
            (dataset[position].getSvgShape() != null) && (dataset[position].getSvgShape() != "") -> {
                holder.view.product_image.setImageDrawable(
                    PictureDrawable(
                        SVG.getFromString(dataset[position].getSvgShape()).renderToPicture()
                    )
                )
            }
            dataset[position] is Matter && (dataset[position] as Matter).france_maaid != null -> {
                holder.view.product_image.setImageDrawable(this.context.getDrawable(R.drawable.ic_uf41d_info_outline))
                holder.view.product_image.layoutParams.width = 50
                holder.view.product_image.requestLayout()
                holder.view.product_image.setOnClickListener {
                    val dialog = PhytoInfoDialogFragment.newInstance((dataset[position] as Matter).france_maaid?.toLong() ?: 0L)
                    dialog.show((this.context as NewInterventionActivity).supportFragmentManager, "PhytoInfoFragment")
                }
            }
            else -> {
                holder.view.product_image.layoutParams.width = 0
                holder.view.product_image.requestLayout()
            }
        }
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}