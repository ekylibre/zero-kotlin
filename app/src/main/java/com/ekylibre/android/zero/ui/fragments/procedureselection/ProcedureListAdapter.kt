package com.ekylibre.android.zero.ui.fragments.procedureselection

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.item_procedure.view.*
import javax.inject.Inject

class ProcedureListAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<ProcedureListAdapter.ProcedureListViewHolder>() {

    private var dataset: ArrayList<ProcedureDB> = ArrayList()
    private var colorId: Int = 0

    class ProcedureListViewHolder(val view: ConstraintLayout) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProcedureListViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_procedure, parent, false) as ConstraintLayout
        return ProcedureListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProcedureListViewHolder, position: Int) {
        holder.view.procedure_button.apply {
            text = Utils.getDisplayString(dataset[position].name, context)
            //setTextColor(Utils.getCustomTextColor(position, context))
        }
        holder.view.procedure_button.setStrokeColorResource(this.colorId)

        val action = ProcedureSelectionFragmentDirections.actionProcedureSelectionFragmentToProcedureFormFragment(dataset[position].name)
        holder.view.procedure_button.setOnClickListener { holder.view.findNavController().navigate(action) }
    }

    fun setProcedureListData(procedureList: List<ProcedureDB>, colorId: Int) {
        this.colorId = colorId
        this.dataset.apply {
            clear()
            if (procedureList[0].position != "")
                addAll(procedureList.sortedBy { if (it.position == "") 1000 else it.position.toInt() })
            else
                addAll(procedureList.sortedBy { Utils.getDisplayString(it.name, context) })
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}