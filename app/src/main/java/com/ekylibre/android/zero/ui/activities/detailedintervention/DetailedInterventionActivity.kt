package com.ekylibre.android.zero.ui.activities.detailedintervention

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.Constants
import com.ekylibre.android.ekyapicom.sdk.SyncState
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.constants.AppConstants
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_detailed_inter_view.*
import kotlinx.android.synthetic.main.fragment_procedure_selection.*
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class DetailedInterventionActivity: AppCompatActivity() {

    private val model: DetailedInterventionViewModel by viewModels()
    @Inject
    lateinit var viewAdapter: DetailedInterventionParamCategoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed_inter_view)
        model.loadInterventionDetails(intent.getLongExtra("inter_id", 0))

        close_detailed_inter_activity_btn.setOnClickListener { finish() }
        if (model.inter!!.intervention.nature != AppConstants.REQUEST)
            confirm_btn.visibility = View.GONE

        val family = model.getProcedureFamily(model.inter!!.intervention.procedure)
        confirm_btn.setOnClickListener {
            Utils.startActivity(this, NewInterventionActivity::class.java, mapOf("state" to 1, "inter_id" to model.interId, "inter_type" to family!!))
            Utils.displaySnackBar(Utils.getDisplayString("inter_created", this), detailed_inter_param_adapter)
            //finish()
        }
        edit_btn.setOnClickListener { Utils.startActivity(this, NewInterventionActivity::class.java, mapOf("inter_id" to model.interId, "inter_type" to family!!)) }

        tv_detailed_inter_name.text = Utils.getDisplayString(model.inter?.intervention?.procedure ?: "", this) + " " + getString(R.string.number) + model.inter?.intervention?.number
        model.inter?.intervention?.startDate?.let { start ->
            detailed_inter_date_tv.text = Utils.getDisplayDate(Date(start))
            model.inter?.intervention?.stopDate.let { stop ->
                detailed_inter_time_tv.text = String.format("%.2f", Utils.getDurationInHours(Date(start), Date(stop!!)))+" ${applicationContext.getString(R.string.hours)}"
            }
        }

        val err = model.getLocalInterventionError(intent.getLongExtra("inter_id", 0))
        if (err != "") {
            imageView8.visibility = View.VISIBLE
            errt.text = err
        }

        model.inter?.intervention?.syncState?.let { state ->
            if (state == SyncState.LOCAL.ordinal) delete_btn.visibility = View.VISIBLE
            delete_btn.setOnClickListener {
                model.delete()
                finish()
            }
        }

        model.getGroupedParams()?.let { viewAdapter.setData(it, model.getTargetsDetails(applicationContext)) }

        model.totalArea?.let { detailed_inter_total_area_tv.text = String.format("%.4f", it.value.toFloat()) + " " + Constants.getUnitString(it.unit, applicationContext) }
        detailed_inter_cost_tv.text = model.inter?.intervention?.totalCost.toString() + "€"

        val viewManager = LinearLayoutManager(applicationContext)
        findViewById<RecyclerView>(R.id.detailed_inter_param_adapter).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }
}