package com.ekylibre.android.zero.ui.fragments.procedureselection

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.constants.ProcedureCategories
import dagger.hilt.android.qualifiers.ActivityContext
import kotlinx.android.synthetic.main.fragment_remote_interventions.*
import kotlinx.android.synthetic.main.item_procedure_category.view.*
import javax.inject.Inject

class ProcedureCategoriesAdapter @Inject constructor(@ActivityContext private val context: Context) : RecyclerView.Adapter<ProcedureCategoriesAdapter.ProcedureCategoriesViewHolder>() {

    private lateinit var proceduresList: List<ProcedureDB>
    private var dataset: ArrayList<ProcedureCategories> = ArrayList()

    class ProcedureCategoriesViewHolder(val view: ConstraintLayout) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProcedureCategoriesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_procedure_category, parent, false) as ConstraintLayout
        return ProcedureCategoriesViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProcedureCategoriesViewHolder, position: Int) {
        holder.view.setBackgroundColor(holder.view.resources.getColor(this.dataset[position].colorId))
        holder.view.filterbtn.setOnClickListener {
            spoiler(holder)
        }
        holder.view.setOnClickListener {
            spoiler(holder)
        }
        holder.view.name_tv.text = context.resources.getString(this.dataset[position].id)
        initRecyclerView(holder.view.procedure_list_recycler, this.dataset[position])
    }

    fun spoiler(holder: ProcedureCategoriesViewHolder) {
        val state = holder.view.procedure_list_recycler.visibility == View.VISIBLE
        val rotate = RotateAnimation(180f,0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 150
        rotate.interpolator = LinearInterpolator()
        holder.view.filterbtn.startAnimation(rotate)

        holder.view.filterbtn.rotation = if(state) 0f else 180f
        holder.view.procedure_list_recycler.visibility = if(state) View.GONE else View.VISIBLE
    }

    override fun getItemCount(): Int {
        return this.dataset.size
    }

    fun setProcedureCategoriesData(procedureCategoriesList: List<ProcedureCategories>?) {
        this.dataset.clear()
        procedureCategoriesList?.let { this.dataset.addAll(procedureCategoriesList) }
        notifyDataSetChanged()
    }

    private fun initRecyclerView(procedureListRecycler: RecyclerView, p: ProcedureCategories) {
        val viewManager = GridLayoutManager(context, 2)
        val procedureListAdapter = ProcedureListAdapter(context)
        procedureListAdapter.setProcedureListData(getProceduresByCategoryName(p.categoryName), p.colorId)
        procedureListRecycler.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = procedureListAdapter
        }
    }

   private fun getProceduresByCategoryName(categoryName: String): ArrayList<ProcedureDB> {
       val res = ArrayList<ProcedureDB>()
       proceduresList.map {
           if (it.categories.split(", ").indexOf(categoryName) != -1)
               res.add(it)
       }

       return res
   }

    fun setProceduresList(allProcedures: List<ProcedureDB>) {
        proceduresList = allProcedures
    }
}
