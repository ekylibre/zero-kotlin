package com.ekylibre.android.zero.constants

import com.ekylibre.android.zero.R

class ProcedureFamilies() {
    companion object {
        val procedureFamilies = mapOf<Int, ArrayList<ProcedureCategories>>(
            R.id.administrative_task_button to arrayListOf(ProcedureCategories.ADMINISTRATIVE_TASK),
            R.id.animal_production_button to arrayListOf(ProcedureCategories.ANIMAL_BREEDING, ProcedureCategories.ANIMAL_IDENTIFICATION, ProcedureCategories.ANIMAL_FEEDING, ProcedureCategories.ANIMAL_PRODUCTION, ProcedureCategories.ANIMAL_TREATING, ProcedureCategories.HABITAT_MAINTENANCE),
            R.id.vege_production_button to arrayListOf(ProcedureCategories.SOIL_WORKING, ProcedureCategories.PLANTING, ProcedureCategories.FERTILIZING, ProcedureCategories.IRRIGATING, ProcedureCategories.CROP_PROTECTION, ProcedureCategories.CROP_MAINTENANCE, ProcedureCategories.HARVESTING),
            R.id.maintenance_task_button to arrayListOf(ProcedureCategories.ENERGY_SUPPLYING, ProcedureCategories.EQUIPMENT_MAINTENANCE, ProcedureCategories.HANDLING, ProcedureCategories.INSTALLATIONS_MODIFICATION),
            R.id.transformation_task_button to arrayListOf(ProcedureCategories.PLANTS_TRANSFORMATION, ProcedureCategories.PACKAGING, ProcedureCategories.VITICULTURE_TRANSFORMATION),
            R.id.wine_production_button to arrayListOf(ProcedureCategories.VINEYARD_MAINTENANCE, ProcedureCategories.VINE_FERTILIZING, ProcedureCategories.VINE_PROTECTION, ProcedureCategories.VINE_SOIL_WORKING_AND_MAINTENANCE, ProcedureCategories.VINE_PLANTING, ProcedureCategories.TRELLISING, ProcedureCategories.VINE_IRRIGATING)
        )
    }
}