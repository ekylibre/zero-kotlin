package com.ekylibre.android.zero.ui.fragments.procedureform.datepicker

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import java.util.*

class DatePickerFragment(var listener: OnDateAdjustedListener, val c: Calendar) : DialogFragment(), DatePickerDialog.OnDateSetListener {

    interface OnDateAdjustedListener {
        fun onDateAdjusted(tag: String?, year: Int, month: Int, day: Int)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        return DatePickerDialog((activity as Context), this, year, month, day)
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
        this.listener.onDateAdjusted(tag, year, month, day)
    }
}