package com.ekylibre.android.zero.ui.activities.settings

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.ekyapicom.sdk.Utils
import android.content.pm.PackageManager

import android.content.pm.PackageInfo




class SettingsViewModel@ViewModelInject constructor(private val ekyAPIComSDK: EkyAPIComSDK): ViewModel() {
    var interSyncPolicy: String = ""
    var settingsKey: String = ""
    var version: String = ""

    var syncProcessed = false
    private var syncResults = HashMap<String, Boolean>()

    fun getCurrentSettings(context: Context) {
        this.settingsKey = Utils.readStringInSharedPreferences("username", context)+"_inter_to_sync"
        this.interSyncPolicy = Utils.readStringInSharedPreferences(settingsKey, context) ?: ""

        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            this.version = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    fun isThemeSwitchChecked(context: Context): Boolean {
        return Utils.readBooleanInSharedPreferences(Utils.readStringInSharedPreferences("username", context)+"_theme", context) ?: false
    }

    fun isRecordSwitchChecked(): Boolean {
        return (interSyncPolicy == "record") || (interSyncPolicy == "both")
    }

    fun isPlannedSwitchChecked(): Boolean {
        return (interSyncPolicy == "request") || (interSyncPolicy == "both")
    }

    fun save(planned: Boolean, record: Boolean, theme: Boolean, context: Context) {
        val newSyncPolicy = if (planned && record) "both"
        else if (planned) "request"
        else if (record) "record"
        else ""

        if (newSyncPolicy != interSyncPolicy) {
            Utils.saveInSharedPreferences(settingsKey, newSyncPolicy, context)
            ekyAPIComSDK.destroyInterventionsInDB()
        }

        Utils.saveInSharedPreferences(Utils.readStringInSharedPreferences("username", context)+"_theme", theme, context)
    }

    fun serverDataSync() {
        ekyAPIComSDK.destroyAll()
        Thread.sleep(100)
        ekyAPIComSDK.apply {
            syncResults = hashMapOf("land_parcels" to false, "workers" to false, "equipments" to false, "plants" to false, "variants" to false, "matters" to false, "building_divisions" to false, "phytos" to false, "phytos_usages" to false, "animals" to false)
            getLandParcelsFromAPI()
            getVariantsFromAPI()
            getEquipmentsFromAPI()
            getPlantsFromAPI()
            getMattersFromAPI()
            getWorkersFromAPI()
            getBuildingDivisionsFromAPI()
            getPhytosFromAPI()
            getPhytoUsagesFromAPI()
            getAnimalsFromAPI()
        }
    }

    fun onSharedPreferenceChanged(key: String?) {
        if (syncResults.isNotEmpty())
            key?.let {
                if (it.contains("last_") && it.contains("_sync"))
                    syncSuccess(it.substring(5, it.length -5))
            }
    }

    fun syncSuccess(syncKey: String) {
        this.syncResults[syncKey] = true
    }

    fun isSyncCompleteAndSuccessfull(): Boolean {
        return if (this.syncResults.values.all { it }) {
            this.syncResults = this.syncResults.mapValues { false } as HashMap<String, Boolean>
            this.syncProcessed = true
            true
        } else false
    }
}