package com.ekylibre.android.zero

import android.app.Application
import android.util.Log
import com.ekylibre.android.ekyapicom.sdk.Utils
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ZeroApplication: Application() {
    override fun onCreate () {
        super.onCreate()

    }
}