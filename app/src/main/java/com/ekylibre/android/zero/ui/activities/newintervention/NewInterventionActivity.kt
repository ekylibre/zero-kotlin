package com.ekylibre.android.zero.ui.activities.newintervention

import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import com.ekylibre.android.ekyapicom.sdk.Constants
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PhytoUsage
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.HandlerParameter
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.ui.fragments.procedureform.datepicker.DatePickerFragment
import com.ekylibre.android.zero.ui.fragments.procedureform.datepicker.TimePickerFragment
import com.ekylibre.android.zero.ui.fragments.procedureselection.ProcedureSelectionFragmentDirections
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_new_intervention.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class NewInterventionActivity: AppCompatActivity(), DatePickerFragment.OnDateAdjustedListener, TimePickerFragment.OnTimeAdjustedListener {

    private val model: NewInterventionViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_intervention)

        val interId = getIntervention()
        if (interId != 0L) {
            val inter = model.loadIntervention(interId ?: 0)
            val action = ProcedureSelectionFragmentDirections.actionProcedureSelectionFragmentToProcedureFormFragment(inter.intervention.procedure, inter.intervention.i_id)
            nav_new_inter_fragment.findNavController().navigate(action)
            model.loadIntervention()

            if (getState() != 0) {
                model.saveIntervention(true)
                finish()
            }
        }
    }

    fun getProcedureFamilyId(): Int? {
        return intent.extras?.getInt("inter_type")
    }

    fun getIntervention(): Long? {
        return intent.extras?.getLong("inter_id")
    }

    fun getState(): Int? {
        return intent.extras?.getInt("state")
    }

    fun getSelectedProductsLiveData(key: String, name: String, isGroup: Boolean, groupId: Int): MutableLiveData<ArrayList<InterItem>> {
        return model.getSelectedProductsLiveData(key, name, isGroup, groupId)
    }

    fun getLoadedUsages(): Map<Long, List<PhytoUsage>> {
        return model.getLoadedUsages()
    }

    override fun onTimeAdjusted(tag: String?, hour: Int, minute: Int) {
        model.updateDate(tag, hour, minute)
    }

    override fun onDateAdjusted(tag: String?, year: Int, month: Int, day: Int) {
        model.updateDate(tag, year, month, day)
    }

    fun setQuantity(productId: Long, quantity: String?, handler: HandlerParameter?, key: String, name: String, isGroup: Boolean, groupId: Int) {
        model.setQuantity(productId, quantity, handler, key, name, isGroup, groupId)
    }

    fun setNewBornDetails(productId: Long, newBornName: String?, idNumber: Long?,  sex: String?, birthCdt: String?, healthy: Boolean?, key: String, name: String, isGroup: Boolean, groupId: Int) {
        model.setNewBornDetails(productId, newBornName, idNumber, Constants.getGenderKeyFromString(sex, applicationContext), Constants.getBirthCdtKeyFromString(birthCdt, applicationContext), healthy, key, name, isGroup, groupId)
    }

    fun updateSelectedProducts(id: Long) {
        model.updateSelectedProducts(id, false)
    }

    fun deleteSelectedProduct(key: String, name: String, productId: Long, isGroup: Boolean, groupId: Int) {
        model.deleteSelectedProduct(key, name, productId, isGroup, groupId)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_save_inter -> {
            model.saveIntervention(false)
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }
}