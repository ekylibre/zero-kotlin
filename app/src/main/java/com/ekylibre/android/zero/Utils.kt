package com.ekylibre.android.zero

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*

class Utils {
    companion object {
        fun <T> startActivity(c: Context, activityClass: Class<T>) {
            val i = Intent(c, activityClass)
            c.startActivity(i)
        }

        fun <T> startActivity(c: Context, activityClass: Class<T>, arguments: Map<String, Number>) {
            val i = Intent(c, activityClass)
            arguments.keys.map { k ->
                i.putExtra(k, arguments[k])
            }
            c.startActivity(i)
        }

        fun <T> startActivity(c: Context, activityClass: Class<T>, key: String, value: Long) {
            val i = Intent(c, activityClass)
            i.putExtra(key, value)
            c.startActivity(i)
        }

        fun isOnline(c: Context): Boolean {
            val cm = c.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            return activeNetwork?.isConnectedOrConnecting == true
        }

        fun timestampToDate(s: String): String {
            val sdf = SimpleDateFormat("dd/MM/yyyy - HH:mm")
            val netDate = Date(s.toLong())

            return sdf.format(netDate)
        }

        private fun initSnackBar(message: String, view: View): Snackbar {
            return Snackbar.make(
                    view, message,
                    Snackbar.LENGTH_LONG
            )
        }

        fun displaySnackBar(message: String, view: View) {
            initSnackBar(message, view).show()
        }

        fun displaySnackBar(message: String, view: View, action: String, listener: View.OnClickListener) {
            val snackBar = initSnackBar(message, view).setAction(action, listener)
            snackBar.show()
        }

        fun getDisplayDate(s: String): String {
            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault())
            val cal = Calendar.getInstance()
            sdf.parse(s)?.let { cal.time = it }
            return SimpleDateFormat("dd MMM yyyy, HH:mm").format(cal.time)
        }

        fun getDisplayDate(c: Calendar): String {
            val outputFmt = SimpleDateFormat("E dd MMM yyyy")
            return outputFmt.format(c.time)//c.get(Calendar.DAY_OF_MONTH).toString() + "/" + (c.get(Calendar.MONTH) +1).toString() + "/" + c.get(Calendar.YEAR).toString()
        }

        fun getDisplayDate(d: Date): String {
            val cal = Calendar.getInstance()
            cal.time = d
            return getDisplayDate(cal)
        }

        fun getDisplayTime(c: Calendar): String {
            val outputFmt = SimpleDateFormat("HH:mm")
            return outputFmt.format(c.time)//c.get(Calendar.HOUR_OF_DAY).toString() + ":" + c.get(Calendar.MINUTE).toString()
        }

        fun getDisplayTime(d: Date): String {
            val outputFmt = SimpleDateFormat("HH:mm")
            return outputFmt.format(d)//c.get(Calendar.HOUR_OF_DAY).toString() + ":" + c.get(Calendar.MINUTE).toString()
        }

        fun getDurationInHours(start: Date, stop: Date): Float {
            val diff: Long = stop.time - start.time
            return diff.toFloat() / 3600000
        }

        fun getDisplayString(s: String, c: Context): String {
            val id = c.resources.getIdentifier(s, "string", c.packageName)

            return try {
                c.resources.getString(id)
            } catch (e: Exception) {
                s
            }
        }

        fun getDisplayIcon(s: String, c: Context): Drawable {
            return when (s) {
                "land_parcel", "cultivation" -> c.getDrawable(R.drawable.ic_land_parcel)!!
                "tractor", "equipment" -> c.getDrawable(R.drawable.ic_tractor)!!
                "animal_housing", "zone", "container", "hive" -> c.getDrawable(R.drawable.ic_barn)!!
                "animal", "child", "mammal_to_milk", "mother", "herd" -> c.getDrawable(R.drawable.ic_cow)!!
                "consumable_part", "food", "food_mix", "matters", "oenological_intrant", "seeds", "eggs", "milk", "corks", "product_to_sort", "sortable", "sorted_product", "sorting_differential", "replacement_part", "prepared_product", "product_to_prepare", "variant", "silage", "protective_sleeves", "protective_canvas", "protective_net", "stakes", "vial", "wire_fence", "residue", "candles", "construction_material", "tunnel" -> c.getDrawable(R.drawable.ic_matter)!!
                "animal_medicine" -> c.getDrawable(R.drawable.ic_medkit)!!
                "water", "oil", "fuel", "energy" -> c.getDrawable(R.drawable.ic_energy)!!
                "doer", "driver", "forager_driver", "mechanic", "operator", "wine_man", "worker", "caregiver", "inseminator" -> c.getDrawable(R.drawable.ic_face)!!
                "plant_medicine" -> c.getDrawable(R.drawable.ic_flask)!!
                "adding_wine", "adding_wine_to_blend", "wine", "wine_blended" -> c.getDrawable(R.drawable.ic_barrel)!!
                "cutter", "electric_pruning", "pre_pruner" -> c.getDrawable(R.drawable.ic_cutter)!!
                "destination_tank", "fermented_juice", "juice_to_ferment", "tank", "tank_for_residue", "tank_for_wine", "wine_to_treat" -> c.getDrawable(R.drawable.ic_storage_tank)!!
                "compressor", "cleaner", "corker", "frost_protection_equipment", "hand_tying", "mechanical_pollinator", "pollination_tool", "weeding_kit", "implement", "milking_robot", "press", "packaging_material" -> c.getDrawable(R.drawable.ic_maintenance)!!
                "tool", "trailed_equipment", "wire_lifter", "lifter", "trailer", "baler", "hay_rake", "mulching_material" -> c.getDrawable(R.drawable.ic_tool_alt)!!
                "soil_loosener", "plow", "stubble_cultivator", "harrow", "hoe", "seedbed_preparator", "hiller", "sieve_shaker" -> c.getDrawable(R.drawable.ic_plow)!!
                "litter", "straw", "straw_bales", "straw_to_bunch" -> c.getDrawable(R.drawable.ic_hay)!!
                "cleaner_product", "disinfectant", "fertilizer", "plant_medicine" -> c.getDrawable(R.drawable.ic_chemical_product)!!
                "sower", "implanter_tool", "spreader" -> c.getDrawable(R.drawable.ic_sower)!!
                "implanter_tool", "vine_planter", "cover_implanter", "uncover", "net_implanter", "net_roll", "tunnel_implanter", "tunnel_remover" -> c.getDrawable(R.drawable.ic_implanter)!!
                "cropper", "forager", "corn_topper" -> c.getDrawable(R.drawable.ic_forager)!!
                "grinder", "mower", "leaves_remover", "shoot_grinder", "hand_drawn", "vine_shoot_extractor", "vine_trimmer", "topper", "trunck_cleaner" -> c.getDrawable(R.drawable.ic_grinder)!!
                "sprayer", "gaz_engine", "steam_engine" -> c.getDrawable(R.drawable.ic_sprayer)!!
                "grape" -> c.getDrawable(R.drawable.ic_grapes)!!
                "vehicle" -> c.getDrawable(R.drawable.ic_truck_pickup)!!
                "bottles", "wine_bottles" -> c.getDrawable(R.drawable.ic_bottles)!!
                "raised_manure" -> c.getDrawable(R.drawable.ic_raised_manure)!!
                "plant" -> c.getDrawable(R.drawable.ic_plant)!!
                "pollinating_insects" -> c.getDrawable(R.drawable.ic_bee)!!
                else -> c.getDrawable(R.drawable.ic_outline_circle_24)!!
            }
        }

        fun getCustomColor(position: Int, context: Context): Int {
            val colors = arrayListOf(R.color.inter_item_background_one, R.color.inter_item_background_two, R.color.inter_item_background_three, R.color.inter_item_background_four, R.color.inter_item_background_five)
            return ContextCompat.getColor(context, colors[position.rem(5)])
        }

        fun getCustomTextColor(position: Int, context: Context): Int {
            val colors = arrayListOf(R.color.inter_item_text_one, R.color.inter_item_text_two, R.color.inter_item_text_three, R.color.inter_item_text_four, R.color.inter_item_text_five)
            return ContextCompat.getColor(context, colors[position.rem(5)])
        }
    }
}