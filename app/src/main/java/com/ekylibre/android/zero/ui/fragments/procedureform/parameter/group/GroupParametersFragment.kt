package com.ekylibre.android.zero.ui.fragments.procedureform.parameter.group

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.ui.fragments.procedureform.parameter.ParametersFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.item_parameter.view.*
import javax.inject.Inject

@AndroidEntryPoint
class GroupParametersFragment: ParametersFragment() {
    @Inject
    lateinit var viewAdapter: GroupParameterCategoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root =  inflater.inflate(R.layout.fragment_procedure_parameters_group, container, false)
        initAdapter()
        if (model.loadGroupProcedureParameters().isNotEmpty())
            root.findViewById<LinearLayout>(R.id.group_header).apply {
                visibility = View.VISIBLE
            }
        initRecyclerView(root, viewAdapter)
        viewAdapter.setRemoveListener {
            val data = model.removeGroupItem(it)
            viewAdapter.setGroupData(data)
        }
        root.findViewById<TextView>(R.id.group_name).apply {
            text = model.groupName
        }
        root.findViewById<ImageButton>(R.id.group_add).apply {
            setOnClickListener {
                val data = model.addGroupItem()
                viewAdapter.setGroupData(data)
            }
        }
        return root
    }

    override fun initAdapter() {
        val data = model.loadGroupProcedureParameters()
        viewAdapter.setData(if (data.isNotEmpty()) data[0] else hashMapOf(), false, 0)
        viewAdapter.setGroupData(data)
    }
}