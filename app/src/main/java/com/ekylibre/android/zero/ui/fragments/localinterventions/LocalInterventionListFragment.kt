package com.ekylibre.android.zero.ui.fragments.localinterventions

import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionToSend
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.ui.activities.settings.SettingsActivity
import com.ekylibre.android.zero.ui.fragments.remoteInterventionList.InterventionListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_local_interventions.*

@AndroidEntryPoint
class LocalInterventionListFragment : Fragment(), SharedPreferences.OnSharedPreferenceChangeListener, LocalInterventionAdapter.OnItemClickListener {

    private val model: InterventionListViewModel by viewModels()

    lateinit var localInterventionListAdapter: LocalInterventionAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_local_interventions, container, false)
        context?.getSharedPreferences(context?.packageName, 0)
                ?.registerOnSharedPreferenceChangeListener(this)

        initLiveData()
        initRecyclerView(root, R.id.local_interventions_list_recycler_view, LocalInterventionAdapter(requireContext(), this@LocalInterventionListFragment), LinearLayoutManager(context))
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        local_settings_btn.setOnClickListener { Utils.startActivity(requireContext(), SettingsActivity::class.java) }
        local_sync_btn.setOnClickListener { refresh() }
        val i = remote_inter_searchview
        //i.setColorFilter(getResources().getColor(R.color.activeMenuButton), android.graphics.PorterDuff.Mode.SRC_IN);
        local_sync_btn.setOnClickListener { refresh() }
    }

    private fun refresh() {
        if (Utils.isOnline(requireContext())) {
            model.serverDataSync()
            model.fullInterventionsRefresh()
            val colorInt = resources.getColor(R.color.ongoing_sync)
            local_sync_btn.imageTintList = ColorStateList.valueOf(colorInt)
            local_sync_btn.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate))
        } else
            model.offlineRefresh()
    }

    override fun onResume() {
        super.onResume()
        this.model.refreshLocalInterventionsFromDB()
    }

    private fun initRecyclerView(root: View, recyclerId: Int, a: LocalInterventionAdapter, manager: LinearLayoutManager) {
        this.localInterventionListAdapter = a
        root.findViewById<RecyclerView>(recyclerId).apply {
            setHasFixedSize(true)
            layoutManager = manager
            adapter = a
        }
    }

    private fun initLiveData() {
        val localInterventionsListObserver = Observer<List<InterventionToSend>> { newList -> onInterventionListChanged(newList) }
        model.localInterventionsListLiveData.observe(viewLifecycleOwner, localInterventionsListObserver)
    }

    private fun onInterventionListChanged(newList: List<InterventionToSend>) {
        localInterventionListAdapter.setData(newList.map {
            val p = if (it.getPreview().isEmpty()) -1L else it.getPreview().toLong()
            DisplayableIntervention(
                    it.procedure_name,
                    Utils.getDisplayDate(it.working_periods_attributes[0].started_at.split("+")[0]),
                    "",
                    model.getPreview(p)
            )
        })

        no_local_tv.visibility = if (newList.isNotEmpty()) View.GONE else View.VISIBLE
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        Log.e("Listener", "LocalInterventionListFragment")
        model.onSharedPreferenceChanged(key)
        if (model.isSyncCompleteAndSuccessfull()) setSuccessIcon()
    }

    private fun setSuccessIcon() {
        val colorInt = resources.getColor(R.color.sync_success)
        local_sync_btn?.imageTintList = ColorStateList.valueOf(colorInt)
        local_sync_btn?.animation = null
    }

    override fun onItemClick(position: Int) {
        model.deleteLocalIntervention(position)
    }
}