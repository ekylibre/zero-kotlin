package com.ekylibre.android.zero.constants

class NetworkConstants {
    companion object {
        const val BASE_URL = "https://demo-innovation.ekylibre.io"
        const val TEST_USERNAME = "admin@ekylibre.org"
        const val TEST_PASSWORD = "12345678"
    }
}