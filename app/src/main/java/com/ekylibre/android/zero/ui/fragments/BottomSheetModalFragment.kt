package com.ekylibre.android.zero.ui.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.edit
import com.ekylibre.android.zero.R
import com.ekylibre.android.zero.Utils
import com.ekylibre.android.zero.ui.activities.newintervention.NewInterventionActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_sheet_modal.*

class BottomSheetModalFragment : BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_modal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupButtons()
    }

    private fun setupButtons() {
        val buttons = arrayListOf(administrative_task_button, wine_production_button, maintenance_task_button, transformation_task_button, animal_production_button, vege_production_button)
        buttons.map { b ->
            b.setOnClickListener {
                context?.let { c -> Utils.startActivity(c, NewInterventionActivity::class.java, mapOf("inter_type" to b.id)) }
                dismiss()
            }
        }

        /*add_account_button.setOnClickListener {
            showAddAccountDialog()
            dismiss()
        }*/
    }

    private fun showAddAccountDialog() {
        val dialog = AddAccountDialogFragment()
        dialog.show(parentFragmentManager, "LoginFragment")
    }
}