package com.ekylibre.android.zero.ui.fragments.procedureselection

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB

class ProcedureSelectionViewModel@ViewModelInject constructor(private val ekyAPIComSDK: EkyAPIComSDK): ViewModel() {

    fun getAllProcedures(): List<ProcedureDB> {
        return ekyAPIComSDK.getAllSimplifiedProcedures()
    }
}