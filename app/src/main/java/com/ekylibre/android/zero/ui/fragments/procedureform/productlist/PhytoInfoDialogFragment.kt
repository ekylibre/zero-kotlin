package com.ekylibre.android.zero.ui.fragments.procedureform.productlist

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.ekyapicom.sdk.filter.Filter
import com.ekylibre.android.zero.R
import com.google.android.material.chip.Chip
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dialog_phyto_info.view.*

@AndroidEntryPoint
class PhytoInfoDialogFragment: DialogFragment() {

    private var phytoId = 0L
    private val model: PhytoInfoViewModel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.dialog_phyto_info, null)

            requireArguments().getLong("phytoId").let { this.phytoId = it }
            retrieveData(view)

            builder.setView(view)
                .setPositiveButton("OK") { _, _ -> }

            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun retrieveData(view: View) {
        val phyto = model.getPhytoInfo(phytoId)
        view.phyto_info_name.text = phyto?.name
        view.phyto_info_firm_name.text = phyto?.firm_name
        view.phyto_info_amm_tv.text = phyto?.france_maaid
        view.phyto_info_function_tv.text = phyto?.natures?.joinToString(", ")
        view.phyto_info_ingredients_tv.text = phyto?.active_compounds?.joinToString("\n")
        view.phyto_info_red_tv.text = (phyto?.in_field_reentry_delay?.div(3600)).toString() + "h"
        phyto?.other_names?.map {
            val chip = Chip(context)
            chip.text = it
            view.phyto_info_other_names_chip_group.addView(chip)
        }

        val a = PhytoUsageListAdapter()
        a.setData(model.getPhytoUsage(this.phytoId))
        view.findViewById<RecyclerView>(R.id.phyto_info_usage_recyvler_view).apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = a
        }

    }

    companion object {
        fun newInstance(phytoId: Long): PhytoInfoDialogFragment {
            return PhytoInfoDialogFragment().apply {
                arguments = bundleOf("phytoId" to phytoId)
            }
        }
    }
}