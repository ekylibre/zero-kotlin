# Organization of the project

This project is divided in two distincts parts:

The Android app itself : [Zero-Kotlin](https://gitlab.com/ekylibre/zero-kotlin)

  Developed in Kotlin. Main libs :

    - Hilt

    - Android Material

The API communication (and DB manipulation) SDK : [EkyAPIComSDK](https://gitlab.com/ekylibre/ekyapicomsdk)

    Developed in Kotlin

    Sample application available for testing purpose

    Main libs :

      - Room

      - Retrofit

# How to run

1. Launch EkyAPIComSDK project in Android Studio

2. Add build configuration “Gradle - assemble”

3. Run

4. Copy the generated `ekyapicomsdk/ekyapicomsdk/build/outputs/aar/ekyapicomsdk-release.aar` into `zero-kotlin/app/libs`

5. Open zero-kotlin project in Android Studio

6. Sync projects with Gradle files

7. Run
