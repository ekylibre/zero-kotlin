# Build and Store publication

## Version number

An Android App needs a version code and a version name, both defined in the app `build.gradle` file.

**Example :**

```gradle
defaultConfig {
    applicationId "com.ekylibre.android.kotlin.zero"
    minSdkVersion 21
    targetSdkVersion 30
    versionCode 1002400
    versionName "1.0.24"
    multiDexEnabled true

    testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }
```

Each Store publication needs an increased version code. Here is the chosen formatting for Zero Android (in Kotlin) : <span style="color:yellow">1</span>.<span style="color:cyan">0</span>.<span style="color:blue">24</span> → <span style="color:yellow">1</span><span style="color:cyan">00</span><span style="color:blue">24</span><span style="color:red">00</span>

The numbers in red are used for beta versions, in order to be able to release up to 99 versions in internal testing, without messing with the production logic.

## Building with Android Studio

1. In Android Studio, go to `Build -> Generate Signed Bundle/APK`
2. Choose `Android App Bundle`, then click `Next`
3. If not already filed, pick the keystore file by clicking `Choose existing`. Type passwords & alias, then click `Next`
4. Chose `release` build variant
5. Click `Finish`
6. Once built, the app is in the `build zero-kotlin/app/release/` folder

> 📘 Generated app is not an APK file anymore, but an AAB one.

> ❌ Do not click `Create New` on the keystore selection interface, unless you know **EXACTLY** what you're doing.

## Publishing on the Play Store

1. Go to the [Google Play Console](https://play.google.com/console/u/0/developers/5630979534267674910/app-list)
2. Select Ekylibre in the app listing
3. On the left pannel, pick the publication channel (internal testing, production, etc..)
4. Click `Create a release`
5. Drag & drop the previously generated AAB file in the dedicated area
6. Fill the fields if needed
7. Save, Exam and Publih
