# Pages

## 1. Lifecycle diagram
This diagram shows in details wich activities and fragments are displayed following each user action. Here is a brief reminder of each object usage.
<object data="DiagramFlow_Zero.pdf" type="application/pdf" width="700px" height="700px">
<embed src="DiagramFlow_Zero.pdf">
<p>See [DiagramFlow_Zero.pdf](DiagramFlow_Zero.pdf)</p>
</embed>
</object>

> ⚠️ **Disclaimer**: All the code snippets bellow are EXAMPLES. Meaning, even if they are named after real Zero App classes, the code was re-written to be easily understandable in a doc.

### a. Activities

An Activity can be seen as a page, displaying a view and allowing some user actions. It is a system object, and has its own lifecycle, each step calling a specific method.

An Activity can access system functions (Bluetooth, Storage, etc..) via the `Context` object created allong.

In the following exemple, the `SplashActivity` object overrides the `AppCompatActivity` one, allowing it to define the activity behaviour during its lifecycle.

```kotlin
class SplashActivity : AppCompatActivity() {
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Do stuff
    }
    
    // Other functions to override belongs here
}
```

> ℹ️ **Doc**: [Activity  |  Android Developers](https://developer.android.com/reference/android/app/Activity)

### b. Fragments

A Fragment lives inside an Activity, displaying a view and allowing some user actions. It is a system object, and has its own lifecycle, each step calling a specific method.

A Fragment can access system functions (Bluetooth, Storage, etc..) via its Activity’s `Context` object.

In the following exemple, the `AccountsFragment` object overrides the `Fragment` one, allowing it to define the fragment behaviour during its lifecycle.

```kotlin
class AccountsFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_accounts, container, false)
        // Do stuff
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Do stuff
    }
    
    // Other functions to override belongs here
}
```

> ℹ️ **Doc**: [Fragment  |  Android Developers](https://developer.android.com/reference/android/app/Fragment)

### c. ViewModels

The Zero application is developped following the MVVN architecture. This paradigm divides the code in different layers:
- Activity/Fragment object : UI objects, can ONLY access the UI. All buttons actions or triggered callbacks should redirect to the ViewModel object.
    - Example :
```kotlin
class LocalInterventionListFragment : Fragment(), SharedPreferences.OnSharedPreferenceChangeListener, LocalInterventionAdapter.OnItemClickListener {

    private val model: InterventionListViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_local_interventions, container, false)

        // The button behaviour is defined in the View (here, a Fragment)
        root.view.add_button.setOnClickListener { getSelectedProductsLiveData("key", "name", true) }
        return root
    }
    
    // But when it needs some processing, it is redirected to the Model
    fun getSelectedProductsLiveData(key: String, name: String, isGroup: Boolean): MutableLiveData<ArrayList<InterItem>> {
        return model.getSelectedProductsLiveData(key, name, isGroup)
    }
    
    ...
}
```
- ViewModel object : UI-agnostic, in charge of all the SDKs calls, storage access. TL;DR : this is the intelligent part.
    - Example :
```kotlin
class InterventionListViewModel @ViewModelInject constructor(private val ekyAPIComSDK: EkyAPIComSDK): ViewModel() {
    fun getSelectedProductsLiveData(key: String, name: String, isGroup: Boolean): MutableLiveData<ArrayList<InterItem>> {
        return if (isGroup)
            this.groupSelectedProducts.getOrPut(key) { HashMap() }.getOrPut(name) { MutableLiveData(ArrayList()) }
        else
            this.generalSelectedProducts.getOrPut(key) { HashMap() }.getOrPut(name) { MutableLiveData(ArrayList()) }
    }
}
```

> ℹ️ **Doc**: [Guide to app architecture  |  Android Developers](https://developer.android.com/jetpack/guide?gclid=Cj0KCQiA6NOPBhCPARIsAHAy2zCrbsiyP9lGWGaVuho1mQPrCk48QLaLDMSbdNix13azTcP1KXCYsagaAnUjEALw_wcB&gclsrc=aw.ds)

