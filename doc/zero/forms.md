# Procedures forms definition

In Ekylibre, each procedure (e.g “Harvesting”, “Spraying”, ..) embbed a certain amount of typed fields. Server-side, those fields are defined by a bunch of XMLs files.

**Example :**

```xml
<?xml version="1.0"?>
<procedures xmlns="http://www.ekylibre.org/XML/2013/procedures">
<procedure name="administrative_tasks" categories="administrative_tasks" actions="administrative">
<parameters>
<target name="land_parcel" filter="is land_parcel and has indicator shape" cardinality="*">
<attribute name="working_zone" default-value="PRODUCT.shape" if="PRODUCT?"/>
</target>
<doer name="responsible" filter="is worker"/>
<input name="consumable_part" filter="is preparation" cardinality="*">
<handler name="population"/>
</input>
</parameters>
</procedure>
</procedures>
```

> ❌ **Problem:** All the informations contained in those files are not usefull client-side. More, they take a lot of storage space.

> ✔️ **Solution:** a Bash + AWK script concatenating all the usefull infos into a single JSON file. Lighter and easier to parse.

**Script usage :**

1. Copy/paste all the XMLs files into `zero-kotlin/procedures_files/procedures/`. Do not forget to delete the previous ones first.
2. Go to `zero-kotlin/procedures_files/` and run the `merge_xmls_to_json.sh` script.
3. The script will create the JSON file and copy it to `zero-kotlin/app/src/main/assets/`
4. Open the generated file content in any editor, copy the content and paste it to some online JSON formatter, to delete all the trailing comas. The file is valid but Android will reject it if the comas are still here. 
5. Increase app’s build number, build and publish it. On start, if the app detects a new build number, it will update the procedures in its database. 

**Same procedure, but in JSON, example :**

```json
{
  "name": "administrative_tasks",
  "categories": "administrative_tasks",
  "actions": "administrative",
  "position": "",
  "parameters": {
    "target": [
      {
        "name": "land_parcel",
        "filter": "is land_parcel and has indicator shape",
        "cardinality": "*",
      },
    ],
    "doer": [
      {
        "name": "responsible",
        "filter": "is worker",
        "cardinality": "",
      },
    ],
    "input": [
      {
        "name": "consumable_part",
        "filter": "is preparation",
        "cardinality": "*",
        "handler": [
          {
            "name": "population",
            "indicator": "",
            "unit": "",
          }
        ]
      }
    ]
  }
}
```

