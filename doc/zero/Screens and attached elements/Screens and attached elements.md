# Screens and attached elements

<details>
  <summary>Login page</summary>

View : `LoginActivity.kt`

Layout : `activity_login.xml`

![Login activity](login.png)

</details>

<details>
  <summary>Main page (three tabs)</summary>

This page is the main app page (activity), containing a fragment view.

|                     | **View(s)**                         | **ViewModel(s)**               | **Other(s)**                                                  | **Layout(s)**                                                        |
|---------------------|-------------------------------------|--------------------------------|---------------------------------------------------------------|----------------------------------------------------------------------|
| Main activity       | `MainActivity.kt`                   |                                |                                                               | `activity_main.xml`                                                  |
| Planning            | `InterventionListFragment.kt`       | `InterventionListViewModel.kt` | `RemoteInterventionListAdapter`                               | `fragment_remote_interventions.xml` ; `item_remote_intervention.xml` |
| Local interventions | `LocalInterventionsListFragment.kt` |                                | `DisplayableIntervention.kt` ; `Local InterventionAdapter.kt` | `fragment_local_interventions.xml` ; `item_local_intervention.xml`   |
| Accounts            | `AccountsFragment.kt`               | `AccountsViewModel.kt`         | `AccountsAdapter.kt`                                          | `fragment_accounts.xml` ; `item_account.xml`                         |
| Bottom pop-up menu  | `BottomSheetMoodalFragment.kt`      |                                |                                                               | `bottom_sheet_modal.xml`                                             |
| Add account pop-up  | `AddAccountDialogFragment.kt`       |                                |                                                               | `dialog_add_account.xml`                                             |

> 📘️ Adapters are containers displaying a list of N elements. It’s linked to a layout used to display each item.

![Planning fragment](planning.png)
![Local interventions fragment](local.png)
![Accounts fragment](accounts.png)
![Bottom sheet menu](add.png)
![Add account pop-up](addaccount.png)

</details>

<details>
  <summary>Settings</summary>

This page is the main app page (activity), containing a fragment view.

| **View**              | **ViewModel**          | **Layout**              |
|-----------------------|------------------------|-------------------------|
| `SettingsActivity.kt` | `SettingsViewModel.kt` | `activity_settings.xml` |

![Settings activity](settings.png)
</details>

<details>
  <summary>New intervention interface</summary>

This activity contains a fragment view, wich will display each intervention declaration form step views, successively.

|                             | **View(s)**                     | **ViewModel(s)**                        | **Other(s)**                                                     | **Layout(s)**                                                                             |
|-----------------------------|---------------------------------|-----------------------------------------|------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| New intervention activity   | `NewInterventionActivity.kt`    | `NewInterventionViewModel.kt`           |                                                                  | `activity_new_intervention.xml`                                                           |
| Procedure type selection    | `ProcedureSelectionFragment.kt` | `ProcedureSelectionViewModel.kt`        | `ProcedureListCategoriesAdapter.kt` ; `ProcedureListAdapter.kt`  | `fragment_procedure_selection.xml` ; `item_procedure_category.xml` ; `item_procedure.xml` |
| Procedure form              | `ProcedureFormFragment.kt`      | > ⚠️ Uses `NewInterventionViewModel.kt` |                                                                  | `fragment_procedure_form.xml`                                                             |
| Generic parameter fragment  | `ParameterFragment.kt`          | > ⚠️ Uses `NewInterventionViewModel.kt` | > 📘️ Refer to the table below for adapters details              | > 📘️ Refer to the table below for adapters details                                       |
| Group parameters fragment   | `GroupParametersFragment.kt`    | > 📘️ Extends `ParameterFragment`       | `GroupParameterGategoryAdapter.kt`                               |                                                                                           |
| General parameters fragment | `GeneralParameterFragment.kt`   | > 📘️ Extends `ParameterFragment`       | `GeneralParameterCategoryAdapter.kt`                             |                                                                                           |
| Time picker fragment        | `TimePickerFragment.kt`         |                                         |                                                                  |                                                                                           |
| Date picker fragment        | `DatePickerFragment.kt`         |                                         |                                                                  |                                                                                           |
| Products list pop-up        | `ProductListDialogFragment.kt`  |                                         | `ProductListAdapter.kt`                                          | `dialog_product_selector.xml` ; `item_product.xml`                                        |
| Phyto infos pop-up          | `PhytoInfoDialogFragment.kt`    | `PhytoInfoViewModel.kt`                 | `PhytoUsageListAdapter.kt`                                       | `dialog_phyto_info.xml` ; `item_phyto_usage.xml`                                          |


| **Adapter**                              | **Item layout**                           |
|------------------------------------------|-------------------------------------------|
| `ParameterCategoryAdapter.kt`            | `item_parameter_category.xml`             |
| `ParameterAdapter.kt`                    | `item_parameter.xml`                      |
| `SelectedNewBornAdapter.kt`              | `item_selected_new_born.xml`              |
| `SimpleSelectedProductAdapter.kt`        | `item_simple_selected_product.xml`        |
| `SelectedPhytoProductsAdapter.kt`        | `item_selected_phyto.xml`                 |
| `SelectedProductsWithQuantityAdapter.kt` | `item_selected_product_with_quantity.xml` |

![Procedure selection screen](procedures.png)
![Available products pop-up](products.png)
![Simple product & selected new born interface](newborn.png)
![Phyto products selector](phytos.png)
![Phyto infos pop-up](phyto_info.png)
![Selected phyto product](selected_phyto.png)

</details>

<details>
  <summary>Detailed remote intervention</summary>

| **View**                          | **ViewModel**                      | **Other(s)**                                                                          | **Layout(s)**                                                                                                     |
|-----------------------------------|------------------------------------|---------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| `DetailedInterventionActivity.kt` | `DetailedInterventionViewModel.kt` | `DetailedInterventionParamCategoryAdapter.kt` ; `DetailedInterventionParamAdapter.kt` | `activity_detailed_inter_view.xml` ; `item_detailed_inter_param.xml` ; `item_detailed_inter_parameter_category.xml` |

![Detailed remote intervention](remote_inter.png)

</details>
