# Tables

| Data Class                                 | DAO file                      | Matching JSON Class                                            | Usage                                                              |
|--------------------------------------------|-------------------------------|----------------------------------------------------------------|--------------------------------------------------------------------|
| `InterventionDB.kt`                        | `InterventionDAO.kt`          | `InterventionResponse.kt`                                      | Used in the remote interventions retrieval.                        |
| `InterventionParameterDB.kt`               | `InterventionParameterDAO.kt` | `Parameter.kt`                                                 | Used in the remote interventions retrieval.                        |
| `InterventionProductDB.kt`                 | `InterventionProductDAO.kt`   | `Product.kt`                                                   | Used in the remote interventions retrieval.                        |
| `InterventionParameterWithProductDB.kt`    |                               |                                                                | Link between InterventionParameterDB and InterventionProductDB     |
| `InterventionWithParameterAndProductDB.kt` |                               |                                                                | Link between InterventionDB and InterventionParameterWithProductDB |
| `InterventionToSendDB.kt`                  | `InterventionToSendDAO.kt`    | `InterventionToSend.kt`                                        | Local intervention stored before server sync.                      |
| `AnimalDB.kt`                              | `AnimalDAO.kt`                | `Animal.kt`                                                    | Animal REST API endpoint data.                                     |
| `BuildingDivisionDB.kt`                    | `BuildingDivisionDAO.kt`      | `BuildingDivision.kt`                                          | Building division REST API endpoint data.                          |
| `EquipmentDB.kt`                           | `EquipmentDAO.kt`             | `Equipment.kt`                                                 | Equipment REST API endpoint data.                                  |
| `LandParcelDB.kt`                          | `LandParcelDAO.kt`            | `LandParcel.kt`                                                | Land parcelREST API endpoint data.                                 |
| `MatterDB.kt`                              | `MatterDAO.kt`                | `Matter.kt`                                                    | Matter REST API endpoint data.                                     |
| `PhytoDB.kt`                               | `PhytoDAO.kt`                 | `Phyto.kt`                                                     | Phyto REST API endpoint data.                                      |
| `PhytoUsageDB.kt`                          | `PhytoUsageDAO.kt`            | `PhytoUsage.kt` ; `Translation.kt` ; `PhytoMentions.kt.kt`     | Phyto usage included in Phyto object.                              |
| `PlantDB.kt`                               | `PlantDAO.kt`                 | `Plant.kt`                                                     | Plant REST API endpoint data.                                      |
| `VariantDB.kt`                             | `VariantDAO.kt`               | `Variant.kt`                                                   | Variant REST API endpoint data.                                    |
| `WorkerDB.kt`                              | `WorkerDAO.kt`                | `Worker.kt`                                                    | Worker REST API endpoint data.                                     |
| `ProcedureDB.kt`                           | `ProcedureDAO.kt`             | `Procedure.kt`                                                 | Object generated from the JSON procedures file.                    |
| `ProcedureHandlerDB.kt`                    | `ProcedureHandlerDAO.kt`      | `HandlerParameter.kt`                                          | Handler parameters attached to procedure.                          |
| `ProcedureParameterDB.kt`                  | `ProcedureParameterDAO.kt`    | `GenericProcedureParameter.kt` ; `ProcedureParameterWithGroup` | Parameters attached to procedures.                                 |
| `ProcedureProductDB.kt`                    | `ProcedureProductDAO.kt`      | `GenericProductParameter.kt`                                   | Products attached to parameters.                                   |
| `AccountDB.kt`                             | `AccountDAO.kt`               | `Account.kt`                                                   | Connected accounts.                                                |
