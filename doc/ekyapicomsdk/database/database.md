# Database

## General behaviour : Database

Each API REST JSON object has its equivalent for database storage. Those objects are in the `database/models` module, and their respectives DAOs in the `database/dao` one.

### Client side usage

1. Call EkyAPIComSDK’s corresponding method (get*FromDB(), with * being your object's name)

2. The method returns a List<*>

```java
class MainActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
  fun fun() {
    ekyAPIComSDK = EkyAPIComSDK(NetworkConstant.BASE_URL, this)
    ekyAPIComSDK.init()
    val list = ekyAPIComSDK.getWorkersFromDB()
  }
}
```

### How to add a new table

1. Create the corresponding data class in the `database/models` module

2. Same but with the DAO file in the database/dao module

3. Add the freshly created data class to the Database.kt entities list

4. Add DAO link to the Database.kt methods list

5. Add the EkyDbSDKobject method

6. Add the EkyAPIComSDK object method, to allow client-side call

### Example : the Worker object

Data class in `database/models/WorkerDB.kt`

```java
@Entity
data class WorkerDB (
    @PrimaryKey val w_id: Long,
    @ColumnInfo(name = "w_name") val name: String,
    @ColumnInfo(name = "w_number") val number: String,
    @ColumnInfo(name = "w_work_number") val work_number: String,
    @ColumnInfo(name = "w_born_at") val born_at: Long,
    @ColumnInfo(name = "w_variety") val variety: String,
    @ColumnInfo(name = "w_abilities") val abilities: String
)
```

Column name has not to be identical to the field var name.

DAO file in `database/dao/WorkerDAO.kt`

Define here all the queries needed.

```java
@Dao
interface WorkerDAO {
    @Query("SELECT * FROM workerdb")
    fun getAll(): List<WorkerDB>

    @Query("SELECT * FROM workerdb WHERE w_variety LIKE :variety")
    fun getByVariety(variety: String): List<WorkerDB>

    @Query("SELECT * FROM workerdb WHERE w_id IN (:workerIds)")
    fun loadAllByIds(workerIds: LongArray): List<WorkerDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg workerDB: WorkerDB)

    @Delete
    fun delete(workerDB: WorkerDB)
}
```

Add the new data class in `database/Database.kt`

```java
@Database(entities = [InterventionDB::class,
    InterventionParameterDB::class, InterventionProductDB::class,
    PlantDB::class, WorkerDB::class,
    EquipmentDB::class, LandParcelDB::class,
    BuildingDivisionDB::class, MatterDB::class, VariantDB::class,
    AccountDB::class, ProcedureDB::class, ProcedureParameterDB::class,
    ProcedureProductDB::class, ProcedureHandlerDB::class,
    InterventionToSendDB::class, PhytoDB::class, PhytoUsageDB::class, AnimalDB::class], version = 1)
```

Do not forget to increase the database version if ANY change is made. Otherwise, Room will panic and crash the app.

Add the new DAO in `database/Database.kt`

```java
abstract class Database: RoomDatabase() {
    ...
    abstract fun workerDAO(): WorkerDAO
    ...
}
```

EkyDbSDK method

```java
fun storeWorker(workerDBList: List<WorkerDB>) {
    val process = GlobalScope.async { workerDBList.map { w -> db.workerDAO().insertAll(w) } }
    runBlocking { process.await() }
    Log.e("Storing ${workerDBList.size} plants", "OK")
}

fun getStoredWorkers(): List<WorkerDB> { return getStoredWorkers(null) }
```

EkyAPIComSDK method

```java
fun getWorkersFromDB(): List<Worker> {
   Log.e("EkyDB", "Pulling workers from local Database...")
   return ekyDbSDK.getStoredWorkers().map { w -> Worker.createFromWorkerDB(w) }
}
```
