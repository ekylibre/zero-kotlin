# Example : Authentication flow

Let’s take a deeper look in the `api` module.

## Authentication

Now, Ekylibre uses a simple token authentication process, but the former click&Farm used the OAuth one, so both were implemented.

This example will be focused on the simple token one.

### Simple token request flow

> ℹ In this example, EkyDbSDK is considered as an external lib

> 📘 **In purple :** calls that have to be done in the app code
> **In green :** external libs usage

| Step  | Calling method                            | Called method                                                                                       |
|-------|-------------------------------------------|-----------------------------------------------------------------------------------------------------|
| 1     | Any                                       | `EkyAPIComSDK constructor(api_url: String, c: Context)`                                             |
| 1.1   | `EkyAPIComSDK constructor`                | `EkyDbSDK constructor(context: Context)`                                                            |
| 1.2   | `EkyAPIComSDK constructor`                | `SimpleAuthModule constructor(apiUrl: String, context: Context, ekyDbSDK: EkyDbSDK)`                |
| 1.3   | `SimpleAuthModule constructor`            | `SimpleTokenManager constructor(apiUrl: String, context: Context, ekyDbSDK: EkyDbSDK)`              |
| 2     | Any                                       | `EkyAPIComSDK setCredentials(username: String, password: String)`                                   |
| 2.1   | `EkyAPIComSDK setCredentials`             | `SimpleAuthModule setCredentials(username: String, password: String)`                               |
| 3     | Any                                       | `EkyAPIComSDK init()`                                                                               |
| 3.1   | `EkyAPIComSDK init`                       | `SimpleAuthModule init()`                                                                           |
| 3.2   | `SimpleAuthModule init`                   | `SimpleAuthModule create()`                                                                         |
| 3.3   | `SimpleAuthModule create`                 | `SimpleTokenManager generateAccessToken()`                                                          |
| 3.4   | `SimpleTokenManager generateAccessToken ` | `ApiInterface getAuthClient(apiUrl: String)`                                                        |
| 3.4.1 | `ApiInterface getAuthClient`              | `ApiInterface getClient(apiUrl: String, null)`                                                      |
| 3.4.2 | `ApiInterface getClient`                  | `Generate the Retrofit object`                                                                      |
| 3.5   | `SimpleTokenManager generateAccessToken`  | `Create ApiInterface via Retrofit`                                                                  |
| 3.6   | `SimpleTokenManager generateAccessToken ` | `SimpleTokenManager enqueueResponse(apiUrl: String, response: Call<APIResponse>, context: Context)` |
| 3.7   | `SimpleTokenManager enqueueResponse  `    | `SimpleTokenManager onResponseCallback(response: Response<APIResponse>)`                            |
| 3.8   | `SimpleTokenManager onResponseCallback `  | `SimpleTokenManager saveAuthSuccess(response: Response<SimpleTokenResponse>)`                       |
| 3.9   | `SimpleTokenManager saveAuthSuccess `     | `EkyDbSDK storeAccounts(accountsList: List<AccountsDB>)`                                            |
| 3.10  | `SimpleTokenManager saveAuthSuccess `     | `Utils saveInSharedPreferences(values: Map<String, String>, context: Context)`                      |
| 3.11  | `SimpleTokenManager saveAuthSuccess `     | `WorkerProfileManager constructor(context: Context, ekyDbSDK: EkyDbSDK)`                            |
| 3.12  | `SimpleTokenManager saveAuthSuccess `     | `WorkerProfileManager pull()` > 📘 From here, please refer to the Managers section.                 |
