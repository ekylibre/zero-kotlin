# General behaviour : REST API

Each REST API endpoint comes with a bunch of objects, in order to format the request, parse the response and store it in the DB.

Client side usage

1. Call EkyAPIComSDK’s corresponding method (`get*FromAPI()`, with `*` being your object's name)
2. Watch SharedPreferences changes. Once the data is retrieved and stored, the last synchronization date is put in a SharedPreferences string.

```kotlin
class MainActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == "last_interventions_sync") {
        // do stuff
        }
        }
        }
```

## How to add a REST API endpoint call

1. Create the corresponding JSON Adapter object

2. Same but with the manager

3. Add the REST API endpoint address and response type in the ApiInterface object

4. Add the EkyAPIComSDK object method

## Example : the Worker object

**Response object JSON Adapter in** `jsonclasses/MyObject.kt`

First need is to know the response format, and create the corresponding object.

Here is a response example :

```json
[
  {
    "id": 243,
    "name": "Yvan JOULIN",
    "number": "P00000000243",
    "work_number": "YJ",
    "born_at": "1984-01-01T00:00:00.000Z",
    "variety": "worker",
    "abilities": [
      "administer_care(animal)",
      "drive(equipment)",
      "milk(mammalia)",
      "move",
      "repair(equipment)"
    ]
  }
]
```

And its corresponding Json class :

```kotlin
@JsonClass(generateAdapter = true)
class Worker(var id: Long, var name: String, var number: String, var work_number: String?, var born_at: Date, var variety: String, var abilities: MutableList<String>): InterItem {}
```

> ⚠️ The constructor’s fields has to match the name of each object’s attribute.

### Endpoint address and description in `api/ApiInterface.kt`

In the ApiInterface is stored all the REST API endpoints, and the corresponding return types.

```kotlin
@GET("/api/v2/products?product_type=workers")
fun getWorkersAsync(@Query("modified_since") modifiedSince: String?): Call<List<Worker>>
```

### Manager class for all inner pipping in `api/dataManagers/MydataManager.kt`

The manager is used to link the Retrofit REST API call with the right data treatment and DB storage. It has to extends the APICommunicationModule object, wich packs all the Retrofit object related stuff. The manager for the previous Worker example is :

```kotlin
class WorkerManager(var context: Context, var ekyDbSDK: EkyDbSDK): APICommunicationModule {

    override fun onResponseCallback(r: Response<APIResponse>) {
        if (r.body() != null && r.isSuccessful) {
            // Once the response successfully received, it's casted as a List<Worker>, and sent to the DB for storage.
            val workersToStore: List<Worker> = r.body()!! as List<Worker>
            ekyDbSDK.storeWorker(workersToStore.map { w -> w.adaptForDb() })
        }
    }

    fun pull() {
        val modifiedSince = Utils.readStringInSharedPreferences("last_workers_sync", context)

        // Here is the call to ApiInterface object, wich return a list of Worker objects
        val response = if (modifiedSince == "") ApiInterface.get(context)?.getWorkersAsync(null) else ApiInterface.get(context)?.getWorkersAsync(modifiedSince)
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, response as Call<APIResponse>, context) }
    }
}
```

### EkyAPIComSDK method

To allow the client app to use this fresh endpoint definition, it has to be available in the EkyAPIComSDK.kt methods.

```kotlin
fun getWorkersFromAPI() {
    Log.e("EkyAPI", "Pulling workers from API...")
    // No networking on UI thread
    thread { WorkerManager(context, ekyDbSDK).pull() }
}
```
