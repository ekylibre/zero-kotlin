# EkyAPIComSDK

In order to make integration projects easier to maintain in the long run, all the Zero “deep” functionnalities have been isolated in a SDK.

Thereby, if there is a need to update anything about API communication or database structure, all apps will be updated the same way.

## Files architecture

1. `ekyapicomsdk/app` : sample application. For testing purpose ONLY.
2. `ekyapicomsdk/ekyapicomsdk/src/main/java/com/ekylibre/android/ekyapicom/sdk/` : actual SDK code
    a. `api` : REST API interface and authentication managers definition
    b. `database` : tables definition, DAOs and API
    c. `filter` : onthology and products filter related stuff
    d. `jsonclasses` : structure of each JSON object used for REST API communication
    e. `ApiComError.kt` : SDK error codes
    f. `Constants.kt` : it's in the name
    g. `EkyAPIComSDK.kt` : SDK's API. Those are the only SDK functions available to use while developing the application itself
    h. `SyncState.kt` : enum of each intervention server synchronization state. Futur proof as it is not yet used (will be if both local and remote interventions could be save in the same DB table)
    i. `Unit.kt` : See f.
    j. `Utils.kt` : See i.
3. `res`
