# Zero-kotlin

Zero is a mobile app link to Ekylibre farm management information system (aka FMIS) web application.

More details can be found on the official website https://ekylibre.com

The source code of Ekylibre is available https://github.com/ekylibre/ekylibre

Zero use Ekylibre API https://ekylibre.stoplight.io/docs/eky/YXBpOjE0NDMwODAz-ekylibre

# Local testing with Ekylibre

Go to your ekylibre folder and launch ekylibre with your tenant :

TENANT=demo foreman s

then go to your home folder and launch ngrok with the correct parameter (port number and host-header)

./ngrok http -host-header=demo.ekylibre.lan 8080

Copy the https link of ngrok and use it on the app mobile as URL credentials

# Documentation

See doc folder for installation and technical documentation

## See also

* [Forum](http://forum.ekylibre.org)
* [User Documentation - FR](https://doc.ekylibre.com/v2/fr/demarrage/)
* [Official Demo](https://demo-innovation.ekylibre.io)
* [Official Demo dataset - FR](https://github.com/ekylibre/first_run-demo)

## Follow us

* [Twitter](https://twitter.com/Ekylibre)
* [Facebook](https://www.facebook.com/ekylibre)
* [YouTube](http://www.youtube.com/channel/UC_yYJGkq-aqC-So8DlXtM5g)

## License

Zero is released under the [GNU/AGPLv3](http://opensource.org/licenses/AGPL-3.0)
license.
